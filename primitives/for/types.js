import PropTypes from "prop-types";

const PROPTYPES = {
  iteratee: PropTypes.any,
  of: PropTypes.any,
  item: PropTypes.string,
  parent: PropTypes.string,
};

const DEFAULT_PROPTYPES = {
  iteratee: undefined,
  of: [],
  item: "p",
  parent: undefined,
};

export { PROPTYPES, DEFAULT_PROPTYPES };
