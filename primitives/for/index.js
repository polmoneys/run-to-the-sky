import React, { useMemo, createElement } from "react";
import { PROPTYPES, DEFAULT_PROPTYPES } from "./types";
import { uniqueId,isXS,isXSEmpty, isObjEmpty } from "@/utils";


const normalizeOutput = (pick, of) => {
  if (!pick) return of;

  const normalizedpick = isXS(pick) ? pick : [pick];
  if (!isXS(of))
    return Object.keys(of)
      .filter((k) => normalizedpick.includes(k))
      .reduce((acc, key) => ((acc[key] = of[key]), acc), {});

  return of.map((v) =>
    Object.keys(v)
      .filter((k) => normalizedpick.includes(k))
      .reduce((acc, key) => ((acc[key] = v[key]), acc), {})
  );
};

const ForITem = ({ as = "p", label, value }) => {
  const Tag = as;
  return (
    <Tag className="flex self main-between">
      <span> {label}</span>
      <span> {value}</span>
    </Tag>
  );
};

const For = ({ pick, of, ...more }) => {
  if (isXS(of) && isXSEmpty(of)) return null;
  if (!isXS(of) && isObjEmpty(of)) return null;

  const datum = useMemo(() => normalizeOutput(pick, of), [pick, of]);
  const items = () => {
    const { children, item } = more;

    if (isXS(datum) && children) return children({ datum });

    if (isXS(datum) && typeof datum[0] === "string") {
      return datum.map((target) =>
        createElement(
          item,
          {
            key: uniqueId()
          },
          target
        )
      );
    }

    if (isXS(datum))
      return datum.map((target) => {
        return Object.entries(target).map(([label, value]) => (
          <ForITem as={item} label={label} value={value} key={uniqueId()} />
        ));
      });

    return Object.entries(datum).map(([label, value]) => (
      <ForITem as={item} label={label} value={value} key={uniqueId()} />
    ));
  };

  const output = () => {
    const { className, parent } = more;
    const Tag = parent;
    return parent ? <Tag className={className}> {items()}</Tag> : items();
  };
  return output();
};

export { For };

For.propTypes = PROPTYPES;
For.defaultProps = DEFAULT_PROPTYPES;
