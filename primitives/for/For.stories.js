import React from "react";
import { For } from "./";
import { Spacer, Separator } from "@/primitives/box";

const Template = () => {
  return (
    <>
      <h1>ST$CK</h1>
      <Spacer />
      <For
        pick={["bonus", "value"]}
        of={[
          {
            label: "alpha",
            value: "124",
            bonus: "M$S",
          },
          {
            label: "beta",
            value: "180",
            bonus: "AMZ",
          },
          {
            label: "delta",
            value: "190",
            bonus: "AAP",
          },
          {
            label: "delta",
            value: "10",
            bonus: "F$B",
          },
        ]}
        item="li"
        parent="ul"
      >
        {({ datum }) =>
          datum.map((item, i) => (
            <li className="flex col" key={i}>
              <p>{item.bonus}</p>
              <Separator.Min />
              <p>{item.value}</p>
              <Spacer />
            </li>
          ))
        }
      </For>
      <Spacer />
      <For
        pick={["bonus", "value"]}
        of={[
          {
            label: "alpha",
            value: "124",
            bonus: "M$S",
          },
          {
            label: "beta",
            value: "180",
            bonus: "AMZ",
          },
          {
            label: "delta",
            value: "190",
            bonus: "AAP",
          },
          {
            label: "delta",
            value: "10",
            bonus: "F$B",
          },
        ]}
        item="li"
        parent="ul"
        className="flex stock-row"
      >
        {({ datum }) =>
          datum.map((item, i) => (
            <li className="flex col" key={i}>
              <p>{item.bonus}</p>
              <Separator.Min />
              <p>{item.value}</p>
              <Spacer />
            </li>
          ))
        }
      </For>
    </>
  );
};

export const ForStory = Template.bind({});
