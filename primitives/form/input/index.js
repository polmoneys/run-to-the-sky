import React, { useEffect, useRef, useState } from "react";
import { InputDefaultProps, InputPropTypes } from "../types";
import { InputLabel } from "./label";
import { noop } from "@/utils";
import { ShowIf } from "../";
import * as yup from "yup";
import { Spacer } from "@/primitives/box";

function Input(props) {
  const { initial, name, onChange, showFixes, ...otherProps } = props;
  const [typing, setInputValue] = useState(initial);
  const [error, setError] = useState(false);
  const [showError, setShowError] = useState(showFixes);
  const inputElement = useRef();
  const [capsLockOn, setCapsLock] = useState(false);

  useEffect(() => {
    const { autofocus } = otherProps;
    if (autofocus) inputElement.current.focus();
  }, []);

  useEffect(() => {
    if (showFixes) setShowError(true);
    else setShowError(false);
  }, [showFixes]);

  const { validation } = otherProps;

  useEffect(() => {
    if (validation) {
      validation
        .validate(typing)
        .then(() => {
          // eslint-disable-line
          setError(false);
        })
        .catch((ValidationError) => {
          setError({
            type: name,
            msg: ValidationError.message,
          });
        });
    }
  }, [typing, name, validation]);

  const onChangeUpdate = (e) => {
    e.persist();
    // setCapsLock(e.getModifierState("CapsLock"));
    setInputValue(e.target.value);
    onChange(e);
  };

  const {
    onBlur,
    onFocus,
    // onPaste,
    autocomplete = false,
    label,
    ...elementAttributes // like required or minlenght or inputmode or id or disabled
  } = otherProps;

  const inputClassNames = assignInputClassNames(props);
  const containerClassNames = assignContainerClassNames(props.className, error);
  return (
    <div className={containerClassNames}>
      <InputLabel {...props} />
      <Spacer />

      <input
        ref={inputElement}
        value={typing}
        name={name}
        className={inputClassNames}
        autoComplete={autocomplete ? "on" : "off"}
        aria-required={otherProps.required || false}
        aria-label={otherProps.required ? "required" : "optional"}
        onChange={onChangeUpdate}
        onFocus={onFocus ? onFocus : noop}
        onBlur={onBlur ? onBlur : noop}
        // onPaste={onPaste ? onPaste : onPasteUpdate}
        {...elementAttributes}
      />

      <ShowIf condition={error && showError}>
        <p input-error="" aria-live="polite">
          {error.msg}
        </p>
      </ShowIf>
    </div>
  );
}

function assignInputClassNames(props) {
  return [props.className, props.s && "is-small", props.xs && "is-xs"]
    .filter(Boolean)
    .join(" ");
}

function assignContainerClassNames(className, o_O) {
  return [className, "input-container", o_O && "error"]
    .filter(Boolean)
    .join(" ");
}
Input.Required = (props) => (
  <Input validation={freeTextValidation} required {...props} />
);
Input.Email = (props) => (
  <Input
    type="email"
    inputmode="email"
    validation={emailValidation}
    {...props}
  />
);

Input.displayName = "DefaultsInput";
Input.propTypes = InputPropTypes;
Input.defaultProps = InputDefaultProps;

export { Input };

const freeTextValidation = yup.string().required("Ce champ est obligatoire");

const emailValidation = yup
  .string()
  .email("Veuillez entrer une adresse email valide")
  .required("Ce champ est obligatoire");

/**

var schemaConfirmPasswordMatch = yup.object().shape({
  email: yup
    .string(),
  emailConfirmation: yup
    .string()
    .test('match',
      'emails do not match',
       function(emailConfirmation) {
         return emailConfirmation === this.parent.email;
       }),
}),

   * */
