import React from "react";

const InputLabel = (props) => (
  <label htmlFor={props.name}>
    {props.label || props.name}{" "}
    <span
      style={{
        fontSize: "70%",
      }}
    >
      *
    </span>
  </label>
);

const InputIcoLabel = ({ error, name, value }) => (
  <div
    className="flex main-between cross-baseline"
    style={{ marginBottom: "var(--forms-padding)" }}
  >
    <label htmlFor={name}>Username</label>
    <div>
      <svg width="24" height="24" viewBox="0 0 24 24">
        {value.length > 0 ? (
          <path
            fill={value.length <= 0 ? "#121212" : error ? "#121212" : "#dd5566"}
            d="M19 20H5a1 1 0 0 0 0 2h14a1 1 0 0 0 0-2z"
          />
        ) : null}
        <g fill={value.length <= 0 ? "#121212" : error ? "#121212" : "#dd5566"}>
          <rect width="24" height="24" opacity="0" />
          <path d="M5 18h.09l4.17-.38a2 2 0 0 0 1.21-.57l9-9a1.92 1.92 0 0 0-.07-2.71L16.66 2.6A2 2 0 0 0 14 2.53l-9 9a2 2 0 0 0-.57 1.21L4 16.91a1 1 0 0 0 .29.8A1 1 0 0 0 5 18zM15.27 4L18 6.73l-2 1.95L13.32 6zm-8.9 8.91L12 7.32l2.7 2.7-5.6 5.6-3 .28z" />
        </g>
      </svg>

      {value.length > 0 ? (
        <svg width="24" height="24" viewBox="0 0 24 24">
          <path
            d="M9.86 18a1 1 0 0 1-.73-.32l-4.86-5.17a1 1 0 1 1 1.46-1.37l4.12 4.39 8.41-9.2a1 1 0 1 1 1.48 1.34l-9.14 10a1 1 0 0 1-.73.33z"
            fill={error ? "#dd5566" : "#121212"}
          />
        </svg>
      ) : null}
    </div>
  </div>
);

export { InputLabel, InputIcoLabel };
