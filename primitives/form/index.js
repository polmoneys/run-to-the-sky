import React, { useState } from "react";
import { CheckBox, CheckBoxMulti } from "./checkbox";
import { Input } from "./input";
import { Radio, RadioGroup } from "./radio";
import { Textarea } from "./textarea";
import { TypeAhead, TypeAheadOption, TypeAheadOptionText } from "./typeahead";
import {
  Select,
  SelectOption,
  SelectOptionGroup,
  SelectOptionGroupLabel,
} from "./select";
/* 
  
  TODO: ADD <SHOWIF/> AND showFixes prop TO TYPEAHEAD/SELECT/CHECKBOX
  
  NOTES
  ----- 
  Maybe https://react-spectrum.adobe.com/react-aria/useFocusWithin.html
  Possible enchancements at the end of this file

*/

function Form(props) {
  const { children, ...extra } = props;
  return (
    <form {...extra} autoCapitalize="sentences">
      {children}
    </form>
  );
}

function useForm({ liveValidation = false }) {
  const [invalid, showFixes] = useState(liveValidation); // show user validation message
  const [awaiting, setAwaiting] = useState(false); // disable form/buttons
  const [success, setSuccess] = useState(false); // 👍
  const [o_O, setError] = useState(false); // 👎

  return [
    {
      awaiting,
      fail: o_O,
      ok: success,
      fixes: invalid,
    },
    {
      showFixes,
      setError,
      setSuccess,
      setAwaiting,
    },
  ];
}

export {
  useForm,
  Form,
  Input,
  CheckBox,
  CheckBoxMulti,
  Radio,
  RadioGroup,
  Textarea,
  Select,
  SelectOption,
  SelectOptionGroup,
  SelectOptionGroupLabel,
  TypeAhead,
  TypeAheadOption,
  TypeAheadOptionText,
};

const ShowIf = ({ children, condition }) => {
  return condition ? children : null;
};

function FormDebug(state) {
  return <pre>{JSON.stringify(state, null, 2)}</pre>;
}

export { ShowIf, FormDebug };

/*

  ENCHANCEMENTS
  -------------

  // trim spaces 
  <X onKeyDown={handleKeyDown} onChange={handleChange}/>

  const handleKeyDown = e => {
    if (e.key === " ") {
      e.preventDefault();
    }
  };

  const handleChange = e => {
    if (e.currentTarget.value.includes(" ")) {
      e.currentTarget.value = e.currentTarget.value.replace(/\s/g, "");
    }
  };

  const onPasteUpdate = e => {
    e.persist();
    const pasted = e.clipboardData.getData("text/plain");
    document.execCommand("insertHTML", false, pasted); // eslint-disable-line
  };

function handlePaste (e) {
    var clipboardData, pastedData;

    // Stop data actually being pasted into div
    e.stopPropagation();
    e.preventDefault();

    // Get pasted data via clipboard API
    clipboardData = e.clipboardData || window.clipboardData;
    pastedData = clipboardData.getData('Text');
    
    // Do whatever with pasteddata
    alert(pastedData);
}
addEventListener('paste', handlePaste);

*/
