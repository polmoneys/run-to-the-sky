import Calendar from "react-calendar";
import React, { useState, useEffect } from "react";
import { VisuallyHidden } from "@react-aria/visually-hidden";
import { IcoChevron } from "@/primitives/icon";
import { uniqueId } from "@/utils";
import { Spacer } from "@/primitives/box";

/*

NOTES
-----
API 
https://github.com/wojtekmaj/react-calendar#user-guide
calendarType
locale

ISSUES
https://github.com/wojtekmaj/react-calendar/issues/358
https://github.com/wojtekmaj/react-calendar/issues/424

RECIPES
https://github.com/wojtekmaj/react-calendar/wiki/Recipes

// DISABLE DATES
import { differenceInCalendarDays } from 'date-fns';

function isSameDay(a, b) {
  return differenceInCalendarDays(a, b) === 0;
}

const disabledDates = [tomorrow, in3Days, in5Days];

function tileDisabled({ date, view }) {
  // Disable tiles in month view only
  if (view === 'month') {
    // Check if a date React-Calendar wants to check is on the list of disabled dates
    return disabledDates.find(dDate => isSameDay(dDate, date));
  }
}

 <Calendar
      tileDisabled={tileDisabled}
    />


    //STYLE DATES 

const datesToAddClassTo = [tomorrow, in3Days, in5Days];

function tileClassName({ date, view }) {
  // Add class to tiles in month view only
  if (view === 'month') {
    // Check if a date React-Calendar wants to check is on the list of dates to add class to
    if (disabledDates.find(dDate => isSameDay(dDate, date))) {
      return 'myClassName';
    }
  }
}

   <Calendar
      tileClassName={tileClassName}
    />

*/

const _Calendar = ({
  label = "",
  onDateChange = () => ({}),
  activeStartDate = new Date(),
}) => {
  const [now, setNow] = useState(activeStartDate);
  const ariaId = uniqueId();

  const onChange = (v) => setNow(v);

  useEffect(() => {
    if (now === activeStartDate) return;
    onDateChange(now);
  }, [now]);

  return (
    <div className="input-container self">
      <p id={ariaId} className="label">
        {label}
      </p>
      <Spacer />
      <Calendar
        aria-labelledby={ariaId}
        onChange={onChange}
        showWeekNumbers
        value={now}
      />
    </div>
  );
};

const now = new Date();
const year = now.getFullYear();
const month = now.getMonth();
const day = now.getDay();
const target = new Date(year - 18, month, day);

_Calendar.Dob = (props) => <_Calendar activeStartDate={target} {...props} />;

export { _Calendar as Calendar };
