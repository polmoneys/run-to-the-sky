import React,{ Fragment, useState,useRef, useEffect } from "react";
import { Button } from "@/primitives/action";
import { Calendar } from "@/primitives/form/calendar";
import { IcoCircle } from "@/primitives/icon";
import { Meta, Story, Source, Canvas } from "@storybook/addon-docs/blocks";
import {
  useForm,
  Form,
  Input,
  CheckBox,
  CheckBoxMulti,
  Radio,
  RadioGroup,
  Switch,
  Textarea,
  FormDebug,
  Select,
  SelectOption,
  SelectOptionGroup,
  SelectOptionGroupLabel,
} from "./";

import {  TypeAhead,
  TypeAheadOption,
  TypeAheadOptionText, } from './typeahead/filter-client';

import { Row,Col, Separator, Spacer } from "@/primitives/box";
import { unwrapXS, XSSplit, XSSortBy, XSidify } from "@/utils";
import LANGUAGES from "../../mocks/languages";


const LEVELS = [ "Beginner","Basic", "Good","Expert" ];

const initialState = [
  {  label: "English", level: LEVELS[2] },
  {  label: "French", level: LEVELS[1] },
];

// TODO: FILTER LANGUAGES IN USERCHOICES OUT OF LANGUAGES ?

const Template = () => {
  // FORM
  const backup = useRef(null);
  const [hasFixes, shouldShowFixes] = useState(initialState.liveValidation);
  const [isDisabled, shouldDisable] = useState(initialState.disabled);
  const [success, setSuccess] = useState(false);
  const [o_O, setError] = useState(false);
  // GOAL
  const [userChoices, setUserChoice] = useState(XSidify(initialState));

  useEffect(() => {
    backup.current = initialState;
    return;
  });

  const undo = () => {
    if (backup.current !== null) return;
    setUserChoice(backup.current);
  };

  const addItem = (label) =>{
    if (userChoices.filter(x => x.label !== label).length === 0) return;
    const nextId = userChoices.length + 1;
    const newItem = { level:LEVELS[3], label, id: nextId };
    setUserChoice((prevState) => [...prevState, newItem]);
  };

  const removeItem = (id) => ()=>{
    const [m, d] = XSSplit(
      userChoices,
      (item) => Number(item.id) === Number(id),
    );
    const withIdFixed = d.map((i, idx) => ({ ...i, id: idx + 1 }));
    setUserChoice(withIdFixed);
  };

  const updateState = (o, id) => {
    const [m, d] = XSSplit(
      userChoices,
      (item) => Number(item.id) === Number(id),
    );
    const oldRef = unwrapXS(m);
    setUserChoice((prevState) => [...d, { ...oldRef, ...o }]);
  };
 
  const updateLanguage = (upcoming) => {
    // const { target, persist } = e;
    // if (persist) e.persist();
    // const { name, value } = target;
    console.log(upcoming)
       
  };

  const updateLevel = (upcoming) => {
    // const { target, persist } = e;
    // if (persist) e.persist();
    // const { name, value } = target;
    console.log(upcoming)
  };

  const save = () => {
    console.log(userChoices);
  };

  return (
    <>
     
      <Form onSubmit={save}>
     
        {userChoices?.map(combo => {

          return(
            <Fragment key={combo.id}>
              
          <Row className="main-between cross-start row-gap">
            <Col className="main-between self-stretch ">
            <p  className="label">{combo.label}</p>
          <Button onPress={removeItem(combo.id)}>Delete</Button>
            </Col>
          <Select
            label="Expertise"
            cb={(v) => updateLevel(v)}
            defaultValue={combo.level}
          >
            {LEVELS.map((c) => (
              <SelectOption key={c} value={c}>
                {c}
              </SelectOption>
            ))}
          </Select>
          </Row>
          <Spacer />
          <Separator />
          <Spacer />

          </Fragment>)
        })}
          

          <Row className="main-between cross-start row-gap">

         <TypeAhead.Languages label="Add new language" 

         cb={label=>addItem(label)}>
            {LANGUAGES.map((c) => (
              <TypeAheadOption key={c.code} value={c.name}>
                <TypeAheadOptionText>{c.name}</TypeAheadOptionText>
              </TypeAheadOption>
            ))}
          </TypeAhead.Languages> 

        
        <Button onPress={save} className="self-stretch">Save user choices</Button>
        </Row>
      </Form>

      <Spacer />
      <Separator />
          <Spacer />

      <FormDebug
        state={{
          userChoices,
        }}
      />
      <Spacer />
      <Separator />
          <Spacer />
    </>
  );
};

export const Diy = Template.bind({});
