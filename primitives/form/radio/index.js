import React, { cloneElement, useState } from "react";
import { RadioPropTypes, RadioDefaultProps } from "../types";
import { ShowIf } from "../";
import { RadioLabel } from "./label";
import { useMediaQuery } from "@/utils/use-mq";

const { count, map } = React.Children;

const roundTo = (n, precision) =>
  Math.round(n * 10 ** precision) / 10 ** precision;

function RadioGroup({
  children,
  labelGroup,
  initial,
  name,
  onChange,
  visibility,
}) {
  const [radioGroupSelected, setSelectedValue] = useState(initial);
  const [error, setError] = useState(false);
  const [showError, setShowError] = useState(visibility);

  const isLandscape = useMediaQuery("(orientation: landscape)");

  const onChangeUpdate = (e) => {
    setSelectedValue(e.target.value);
    onChange({ target: { name, value: e.target.value } });
  };

  return (
    <fieldset>
      <legend>{labelGroup}</legend>
      <div className="radio-container">
        {map(children, (c) => {
          const { name } = c.props;
          const checked = radioGroupSelected === name ? true : false;
          return cloneElement(c, {
            ...c.props,
            name,
            checked,
            onChange: onChangeUpdate,
            flexBase: isLandscape ? roundTo(100 / count(children), 0) : 25,
          });
        })}
      </div>
      <ShowIf condition={error && showError}>
        <p input-error="" aria-live="polite">
          {error.msg}
        </p>
      </ShowIf>
    </fieldset>
  );
}

RadioGroup.displayName = "DefaultsRadioGroup";
export { RadioGroup };

function Radio(props) {
  const { checked, name, disabled = false, flexBase, onChange } = props;
  return (
    <div
      className="radio"
      style={{
        position: "relative",
        flex: `1 0 ${flexBase}%`,
      }}
    >
      <div className="radio-pinned">
        <RadioLabel {...props} />
      </div>
      <input
        disabled={disabled}
        name={name}
        value={name}
        checked={checked}
        type="radio"
        onChange={onChange}
      />
    </div>
  );
}

Radio.displayName = "DefaultsRadio";
Radio.propTypes = RadioPropTypes;
Radio.defaultProps = RadioDefaultProps;

export { Radio };
