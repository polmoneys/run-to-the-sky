import React, { forwardRef, useEffect, useState } from "react";
import {
  Listbox,
  ListboxInput,
  ListboxButton,
  ListboxPopover,
  ListboxList,
  ListboxGroup,
  ListboxGroupLabel,
  ListboxOption,
} from "@reach/listbox";
import { VisuallyHidden } from "@react-aria/visually-hidden";
import { IcoChevron } from "@/primitives/icon";
import { uniqueId } from "@/utils";
import { Spacer ,Row} from "@/primitives/box";

const Select = ({
  defaultValue,
  label = "",
  className,
  children,
  cb = () => ({}),
}) => {
  const [value, setValue] = useState(()=>defaultValue);

  const ariaId = uniqueId();

  useEffect(() => {
    if (value === defaultValue) return;
    cb(value);
  }, [value]);

  const classNames = ["input-container self", className]
    .filter(Boolean)
    .join(" ");

  return (
    <div className={classNames}>
   
      <p id={ariaId} className="label">
        {label}
      </p>
      <Spacer />
      <ListboxInput aria-labelledby={ariaId} value={value} onChange={setValue}>
        <ListboxButton>
          {({ value, isExpanded }) => {
            return (
              <>
                {value}
                <IcoChevron
                  size={28}
                  viewbox="-2 -4 28 28"
                  transforms={
                    !isExpanded ? undefined : "rotate(180deg) translate(-2px)"
                  }
                />
              </>
            );
          }}
        </ListboxButton>

        <ListboxPopover>
          <ListboxList>{children}</ListboxList>
        </ListboxPopover>
      </ListboxInput>
    </div>
  );
};

export {
  Select,
  ListboxOption as SelectOption,
  ListboxGroup as SelectOptionGroup,
  ListboxGroupLabel as SelectOptionGroupLabel,
};
