import React, { useState, useRef, useEffect } from "react";
import {
  CustomCheckbox,
  CustomCheckboxContainer,
  CustomCheckboxInput,
  MixedCheckbox,
  useMixedCheckbox,
} from "@reach/checkbox";
import { VisuallyHidden } from "@react-aria/visually-hidden";
import { Spacer } from "@/primitives/box";

const CheckBox = ({
  disabled = false,
  label = "",
  value = "",
  checked = false,
  cb = () => ({}),
}) => {
  return (
    <div className="input-container self">
      <label className="flex row-gap main-start cross-center">
        <CustomCheckbox
          disabled={disabled}
          value={value}
          onChange={(event) => cb(event.target.checked)}
          checked={checked}
        />
        <Spacer />
        {label}
      </label>
      <Spacer />
    </div>
  );
};

export { CheckBox };

const CheckBoxMulti = (props) => {
  const [selectedFilters, setFilters] = useState(props.initialState);
  // We can determine if all or some of the nested checkboxes are selected and
  // use that to determine the state of our parent checkbox.
  const allFiltersChecked = Object.keys(selectedFilters).every(
    (filter) => selectedFilters[filter] === true,
  );
  const someFiltersChecked = allFiltersChecked
    ? false
    : Object.keys(selectedFilters).some(
        (filter) => selectedFilters[filter] === true,
      );
  const parentIsChecked = allFiltersChecked
    ? true
    : someFiltersChecked
    ? "mixed"
    : false;
  function handleParentChange(event) {
    setFilters(
      Object.keys(selectedFilters).reduce(
        (state, filter) => ({
          ...state,
          [filter]: !allFiltersChecked,
        }),
        {},
      ),
    );
  }
  function handleChildChange(event) {
    const { checked, value } = event.target;
    setFilters({
      ...selectedFilters,
      [value]: checked,
    });
  }

  useEffect(() => {
    // cb(selectedFilters)
  }, [selectedFilters]);
  return (
    <div className="input-container self">
      <label className="flex row-gap main-start cross-center">
        <MixedCheckbox
          value="filters"
          checked={parentIsChecked}
          onChange={handleParentChange}
        />
        <Spacer />
        {allFiltersChecked ? "Unselect" : "Select"} all filters
      </label>
      <Spacer />
      <fieldset>
        <VisuallyHidden elementType="legend">Filters</VisuallyHidden>
        <ul>
          {Object.entries(selectedFilters).map(([value, state]) => (
            <li key={value}>
              <label className="flex row-gap main-start cross-center">
                <CustomCheckbox
                  name="filter"
                  value={value}
                  checked={state}
                  onChange={handleChildChange}
                />
                <Spacer />
                {value}
              </label>
              <Spacer />
            </li>
          ))}
        </ul>
      </fieldset>
    </div>
  );
};

export { CheckBoxMulti };
