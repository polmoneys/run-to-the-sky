import React, { useEffect, useRef, useState } from "react";
import { ShowIf } from "../";
import { noop } from "@/utils";

function Textarea(props) {
  const { initial, name, onChange, showFixes, ...otherProps } = props;
  const [typing, setInputValue] = useState(initial);
  const [error, setError] = useState(false);
  const [showError, setShowError] = useState(showFixes);
  const inputElement = useRef();
  const [capsLockOn, setCapsLock] = useState(false);

  useEffect(() => {
    const { autofocus } = otherProps;
    if (autofocus) inputElement.current.focus();
  }, []);

  useEffect(() => {
    if (showFixes) setShowError(true);
    else setShowError(false);
  }, [showFixes]);

  const onChangeUpdate = (e) => {
    e.persist();
    setCapsLock(e.getModifierState("CapsLock"));
    setInputValue(e.target.value);
    onChange(e);
  };

  const {
    onBlur,
    onFocus,
    // onPaste,
    label,
    ...elementAttributes // like required or minlenght or inputmode or id or disabled
  } = otherProps;

  const inputClassNames = assignClassNames(props);
  const containerClassNames = error
    ? "textarea-container error"
    : "textarea-container";
  return (
    <div className={containerClassNames}>
      <label>{label} </label>

      <textarea
        ref={inputElement}
        value={typing}
        name={name}
        className={inputClassNames}
        onChange={onChangeUpdate}
        onFocus={onFocus ? onFocus : noop}
        onBlur={onBlur ? onBlur : noop}
        // onPaste={onPaste ? onPaste : onPasteUpdate}
        {...elementAttributes}
      />

      <ShowIf condition={error && showError}>
        <p input-error="" aria-live="polite">
          {error.msg}
        </p>
      </ShowIf>
    </div>
  );
}

function assignClassNames(props) {
  return ["textarea", props.className, props.s && "-s", props.xs && "-xs"]
    .filter(Boolean)
    .join(" ");
}

export { Textarea };
