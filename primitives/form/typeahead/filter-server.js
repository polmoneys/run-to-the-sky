import React,{useMemo,useState,useRef} from "react";
import {
  Combobox,
  ComboboxInput,
  ComboboxPopover,
  ComboboxList,
  ComboboxOption,
  ComboboxOptionText,
} from "@reach/combobox";
import { uniqueId } from "@/utils";
import { Spacer } from "@/primitives/box";
import {matchSorter} from "match-sorter";
import LANGUAGES from "../../../mocks/languages";
import { useThrottle} from "@/utils/use-throttle"

/*

const TypeAheadEndpoints = (props) => {
  const [query, setQuery] = useState("");
  const debouncedQuery = useDebouncedValue(query, 400);

  const onChange = (event) => setQuery(event.target.value);

  const filteredEmployees = employees.filter((name) => {
    return name.toLowerCase().includes(debouncedQuery);
  });

  return filteredEmployees;
};

export { TypeAheadEndpoints };


TODO:

const TypeAheadFetch  https://reach.tech/combobox/
const debounced at './debounced'

const found = (str) =>arr.find((element) =>
    element.toLowerCase().startsWith(str)
  );
function returnIndexOfFound(str){
 return found(str) ? arr.indexOf(str) : null;
}
console.log(returnIndexOfFound('pe'))

ALTERNATIVE
-----

() => {
  const WAIT = 500;
  const [searchTerm, setSearchTerm] = useState('');
  const [results, setResults] = useState([]);
  const [isSearching, setIsSearching] = useState(false);
  const debouncedSearchTerm = useDebouncedValue(searchTerm, WAIT);
  // API call
  useEffect(
    () => {
      if (debouncedSearchTerm) {
        setIsSearching(true);
        searchCharacters(debouncedSearchTerm).then(results => {
          setIsSearching(false);
          setResults(results);
        });
      } else {
        setResults([]);
      }
    },
    [debouncedSearchTerm]
  );
};
*/


const TypeAhead = ({
  className = undefined,
  chunk = 30,
  placeholder = undefined,
  label = "",
  openOnFocus = true,
  selectOnClick = false,
  portal = false,
  persistSelection = false,
  cb = () => ({}),
}) => {
  const ariaId = uniqueId();

  const classNames = ["input-container self", className]
    .filter(Boolean)
    .join(" ");


  

  return (
    <div className={classNames}>
      <p id={ariaId} className="label">
        {label}
      </p>
      <Spacer />

      <Combobox
        aria-labelledby={ariaId}
        openOnFocus={openOnFocus}
        onSelect={cb}
        className={className}
      >
   
      </Combobox>
    </div>
  );
};

export {
  TypeAhead,
  ComboboxOption as TypeAheadOption,
  ComboboxOptionText as TypeAheadOptionText,
};

