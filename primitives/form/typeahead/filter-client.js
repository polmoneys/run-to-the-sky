import React,{useMemo,useState,useRef} from "react";
import {
  Combobox,
  ComboboxInput,
  ComboboxPopover,
  ComboboxList,
  ComboboxOption,
  ComboboxOptionText,
} from "@reach/combobox";
import { uniqueId } from "@/utils";
import { Spacer } from "@/primitives/box";
import {matchSorter} from "match-sorter";
import LANGUAGES from "../../../mocks/languages";
import { useThrottle} from "@/utils/use-throttle"
import COUNTRIES from "../../../mocks/countries";

// TWO VARIANTS variant === "nationality" or else 

function useQueryLanguageMatch(term) {
  let throttledTerm = useThrottle(term, 100);
  return useMemo(
    () =>
      term.trim() === ""
        ? null
        : matchSorter(LANGUAGES, term, {
            keys: [item => item.name],
          }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [throttledTerm]
  );
}

function useQueryCountryMatch(term) {
  let throttledTerm = useThrottle(term, 100);
  return useMemo(
    () =>
      term.trim() === ""
        ? null
        : matchSorter(COUNTRIES, term),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [throttledTerm]
  );
}


const TypeAhead = ({
  className = undefined,
  chunk = 30,
  variant="nationality",
  placeholder = undefined,
  label = "",
  openOnFocus = true,
  selectOnClick = false,
  portal = false,
  persistSelection = false,
  cb = () => ({}),
}) => {
  const ariaId = uniqueId();

  const classNames = ["input-container self", className]
    .filter(Boolean)
    .join(" ");

    const [term, setTerm] = useState("");
    const results = variant === "nationality" ? useQueryCountryMatch(term) : useQueryLanguageMatch(term);
    const ref = useRef();
  
    const handleChange = event => {
      setTerm(event.target.value);
    };
  
    const handleSelect = value => {
      cb(value);
      setTerm("");
    };
  

  return (
    <div className={classNames}>
      <p id={ariaId} className="label">
        {label}
      </p>
      <Spacer />

      <Combobox
        aria-labelledby={ariaId}
        openOnFocus={openOnFocus}
        onSelect={handleSelect}
        className={className}
      >
        <ComboboxInput
          selectOnClick={selectOnClick}
          placeholder={placeholder}
          ref={ref}
          value={term}
          onChange={handleChange}
          autocomplete={false}
          style={{ width: 400 }}
        />
           {results && (
        <ComboboxPopover portal={portal}>
           {results.length === 0 && (
              <p>
                No Results{" "}
                <button
                  onClick={() => {
                    setTerm("");
                    ref.current.focus();
                  }}
                >
                  clear
                </button>
              </p>
            )}
          <ComboboxList persistSelection={persistSelection}>
          {results.slice(0, chunk).map((result, index) => (
                <ComboboxOption
                  key={index}
                  value={variant === "nationality" ? result : result.name}
                />
              ))}
          </ComboboxList>
        </ComboboxPopover>
         )}
      </Combobox>
    </div>
  );
};

TypeAhead.Languages = (props) => <TypeAhead {...props} variant="languages" />;

export {
  TypeAhead,
  ComboboxOption as TypeAheadOption,
  ComboboxOptionText as TypeAheadOptionText,
};

