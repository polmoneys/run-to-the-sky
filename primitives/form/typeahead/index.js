import React from "react";
import {
  Combobox,
  ComboboxInput,
  ComboboxPopover,
  ComboboxList,
  ComboboxOption,
  ComboboxOptionText,
} from "@reach/combobox";
import { uniqueId } from "@/utils";
import { Spacer } from "@/primitives/box";

const TypeAhead = ({
  className = undefined,
  children,
  placeholder = undefined,
  label = "",
  openOnFocus = true,
  selectOnClick = false,
  portal = false,
  persistSelection = false,
  cb = () => ({}),
}) => {
  const ariaId = uniqueId();

  const classNames = ["input-container self", className]
    .filter(Boolean)
    .join(" ");

  return (
    <div className={classNames}>
      <p id={ariaId} className="label">
        {label}
      </p>
      <Spacer />

      <Combobox
        aria-labelledby={ariaId}
        openOnFocus={openOnFocus}
        onSelect={cb}
        className={className}
      >
        <ComboboxInput
          selectOnClick={selectOnClick}
          placeholder={placeholder}
        />
        <ComboboxPopover portal={portal}>
          <ComboboxList persistSelection={persistSelection}>
            {children}
          </ComboboxList>
        </ComboboxPopover>
      </Combobox>
    </div>
  );
};

export {
  TypeAhead,
  ComboboxOption as TypeAheadOption,
  ComboboxOptionText as TypeAheadOptionText,
};
