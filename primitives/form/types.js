import PropTypes from "prop-types";
import { noop } from "../../utils";

const FormPropTypes = {
  children: PropTypes.any,
};

const FormDefaultProps = {
  children: null,
};

export { FormPropTypes, FormDefaultProps };

const InputPropTypes = {
  label: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  onPaste: PropTypes.func,
  type: PropTypes.string,
};

const InputDefaultProps = {
  label: "INPUT LABEL MISSING",
  onBlur: noop,
  onFocus: noop,
  onPaste: noop,
  type: "text",
};

export { InputDefaultProps, InputPropTypes };

const CheckBoxPropTypes = {
  label: PropTypes.string.isRequired,
  icon: PropTypes.any,
  disabled: PropTypes.bool,
};

const CheckBoxDefaultProps = {
  icon: false,
  label: "",
  disabled: false,
};

export { CheckBoxPropTypes, CheckBoxDefaultProps };

const RadioPropTypes = {
  label: PropTypes.string.isRequired,
  labelGroup: PropTypes.string,
};

const RadioDefaultProps = {
  label: "",
  labelGroup: "",
};

export { RadioPropTypes, RadioDefaultProps };

const SwitchPropTypes = {
  label: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.node,
    PropTypes.string,
  ]),
  onColor: PropTypes.string,
  offColor: PropTypes.string,
  offCb: PropTypes.func,
  onCb: PropTypes.func,
};

const SwitchDefaultProps = {
  label: "",
  onColor: "#ff5555",
  offColor: "rgb(240, 231, 71)",
  offCb: noop(),
  onCb: noop(),
};
export { SwitchPropTypes, SwitchDefaultProps };
