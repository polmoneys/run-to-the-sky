export const CONTENT = Object.freeze({
  "en-EN": {
    invalid: "{field} is not valid",
  },
  "en-US": {
    invalid: "{field} is not valid",
  },
  "fr-FR": {
    invalid: "{field} is not valid",
  },
  "es-ES": {
    invalid: "{field} is not valid",
  },
});
