import PropTypes from "prop-types";
import { IMAGE_PLACEHOLDER } from "@/tokens";

const PROPTYPES = {
  alt: PropTypes.string,
  src: PropTypes.string,
  composeClassName: PropTypes.string,
  className: PropTypes.string,
  placeholder: PropTypes.string,
};

const DEFAULT_PROPTYPES = {
  src: "",
  placeholder: IMAGE_PLACEHOLDER,
  alt: " ",
  composeClassName: undefined,
  className: undefined,
};

export { PROPTYPES, DEFAULT_PROPTYPES };
