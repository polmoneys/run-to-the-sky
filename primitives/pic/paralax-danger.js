import { Parallax } from "react-scroll-parallax";

const ParallaxPic = ({ children, ...elementProps }) => {
  //  y={[-20, 20]} x={[-20, 20]}
  // disabled
  // tagInner tagOuter
  return (
    <Parallax
      offsetYMax={70}
      offsetYMin={-70}
      offsetXMax={-30}
      offsetXMin={30}
      {...elementProps}
    >
      <h1>TESTTT</h1>
    </Parallax>
  );
};

const ParallaxBanner = ({ children, ...elementProps }) => {
  return (
    <ParallaxBanner
      layers={[
        {
          image:
            "https://via.placeholder.com/1600x900/444444/f9f9f9/?text=PIC+SAMPLE",
          amount: 0.1,
        },
        {
          image:
            "https://via.placeholder.com/160x90/DDDDDD/BBBBBB/?text=PIC+SAMPLE",
          amount: 0.2,
        },
      ]}
      style={{
        height: "500px",
      }}
    >
      <h1>Banner Children</h1>
    </ParallaxBanner>
  );
};
