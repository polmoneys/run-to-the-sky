import { PROPTYPES, DEFAULT_PROPTYPES } from "./types";
import Image from "next/image";

const Pic = (props) => {
  const { src, fallback, placeholder, ...validProps } = props;

  const handleBrokenImage = (event) => (event.target.src = placeholder); // TODO:

  const { alt, className, composeClassName, ...extra } = validProps;
  const classNames = [className, composeClassName].filter(Boolean).join(" ");

  return (
    <Image
      className={classNames}
      alt={alt}
      src={src}
      {...extra}
      onError={handleBrokenImage}
    />
  );
};

Pic.Flag = (props) => <Pic {...props} composeClassName="is-flag" />;
Pic.Logo = () => <Pic src="/logo.png" />;
Pic.LogoDashboard = () => <Pic src="/logo-private.png" />;
Pic.Landscape = (props) => <Pic composeClassName="pic-landscape" {...props} />;
Pic.Square = (props) => <Pic composeClassName="pic-square" {...props} />;
Pic.Portrait = (props) => <Pic composeClassName="pic-portrait" {...props} />;
Pic.Avatar = (props) => (
  <Pic composeClassName="pic-avatar" {...props} width="70" height="70" />
);
Pic.AvatarXL = (props) => (
  <Pic
    composeClassName="pic-avatar is-large"
    {...props}
    width="100"
    height="100"
  />
);

// function sourceIsNotAllowed(url) {
//   return !url.startsWith("https") || !url.startsWith("./") ? false : true;
// }

Pic.propTypes = PROPTYPES;
Pic.defaultProps = DEFAULT_PROPTYPES;

export { Pic };
