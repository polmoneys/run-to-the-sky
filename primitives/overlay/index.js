import React from "react";
import { OverlayContainer } from "@react-aria/overlays";
import { Modal } from "./modal";
import { Tray } from "./tray";
import { Toast } from "./toast";
import { PROPTYPES, DEFAULT_PROPTYPES } from "./types";

const Overlay = (props) => {
  const { variant, ...extra } = props;
  return (
    <OverlayContainer>
      {
        {
          modal: <Modal {...extra} />,
          tray: <Tray {...extra} />,
          toast: <Toast {...extra} />,
        }[variant]
      }
    </OverlayContainer>
  );
};

Overlay.Toast = (props) => <Overlay variant="toast" {...props} />;
Overlay.Modal = (props) => <Overlay variant="modal" {...props} />;
Overlay.ModalXS = (props) => (
  <Overlay variant="modal" composeClassName="is-xs" {...props} />
);
Overlay.Tray = (props) => <Overlay variant="tray" {...props} />;
Overlay.TrayMax = (props) => (
  <Overlay variant="tray" composeClassName="is-max" {...props} />
);

Overlay.propTypes = PROPTYPES;
Overlay.defaultProps = DEFAULT_PROPTYPES;

export { Overlay };
