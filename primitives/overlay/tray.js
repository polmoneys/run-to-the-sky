import React, { useEffect, useState, useRef } from "react";
import {
  useOverlay,
  usePreventScroll,
  useModal,
  DismissButton,
} from "@react-aria/overlays";
import { useDialog } from "@react-aria/dialog";
import { FocusScope } from "@react-aria/focus";
import { useViewportSize } from "@/utils/use-viewport-size"; // BECAUSE STORYBOOK
import { Button } from "@/primitives/action";

function Tray(props) {
  const { title, children, className, composeClassName, isOpen } = props;
  const trayClassNames = [
    "tray",
    isOpen && "is-open",
    className,
    composeClassName,
  ]
    .filter(Boolean)
    .join(" ");

  const ref = React.useRef();
  const { overlayProps } = useOverlay(props, ref);

  usePreventScroll();
  const { modalProps } = useModal();
  const { dialogProps, titleProps } = useDialog(props, ref);
  const viewport = useViewportSize();
  const [height, setHeight] = useState(viewport.height);
  const timeoutRef = useRef();

  useEffect(() => {
    clearTimeout(timeoutRef.current);

    // When the height is decreasing, and the keyboard is visible
    // (visual viewport smaller than layout viewport), delay setting
    // the new max height until after the animation is complete
    // so that there isn't an empty space under the tray briefly.
    if (viewport.height < height && viewport.height < window.innerHeight) {
      timeoutRef.current = setTimeout(() => {
        setHeight(viewport.height);
      }, 500);
    } else {
      setHeight(viewport.height);
    }
  }, [height, viewport.height]);

  return (
    <div className="underlay">
      <FocusScope contain restoreFocus autoFocus>
        <div
          {...overlayProps}
          {...dialogProps}
          {...modalProps}
          ref={ref}
          className={trayClassNames}
          style={{
            "--viewport-height": height + "px",
          }}
        >
          <p {...titleProps} className="tray-title">
            {title}
          </p>
          {children}
        </div>
      </FocusScope>
    </div>
  );
}

export { Tray };
