import React from "react";
import {
  useOverlay,
  usePreventScroll,
  useModal,
  DismissButton,
} from "@react-aria/overlays";
import { useDialog } from "@react-aria/dialog";
import { FocusScope } from "@react-aria/focus";
// import { CONTENT } from '@/translations/toast';

// TODO: https://github.com/adobe/react-spectrum/blob/main/packages/%40react-aria/toast/src/useToast.ts

function Toast(props) {
  const { title, children, className, composeClassName } = props;
  const modalClassNames = ["toast", className, composeClassName]
    .filter(Boolean)
    .join(" ");

  const ref = React.useRef();
  const { overlayProps } = useOverlay(props, ref);

  usePreventScroll();
  const { modalProps } = useModal();

  const { dialogProps, titleProps } = useDialog(props, ref);

  return (
    <FocusScope contain restoreFocus autoFocus>
      <div
        // role="alert"
        // role="status"
        // aria-live="polite"
        // aria-relevant="additions"
        {...overlayProps}
        {...dialogProps}
        {...modalProps}
        ref={ref}
        className={modalClassNames}
        // tabIndex="0"
        // role="status"
        // aria-live="polite"
        // aria-relevant="additions"
      >
        <p {...titleProps} className="toast-title">
          {title}
        </p>
        {children}
      </div>
    </FocusScope>
  );
}

export { Toast };
