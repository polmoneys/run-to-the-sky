import React from "react";

import {
  useOverlay,
  usePreventScroll,
  useModal,
  DismissButton,
} from "@react-aria/overlays";
import { useDialog } from "@react-aria/dialog";
import { FocusScope } from "@react-aria/focus";

function Modal(props) {
  const { title, children, className, composeClassName } = props;
  const modalClassNames = ["modal", className, composeClassName]
    .filter(Boolean)
    .join(" ");

  // Handle interacting outside the dialog and pressing
  // the Escape key to close the modal.
  const ref = React.useRef();
  const { overlayProps } = useOverlay(props, ref);

  // Prevent scrolling while the modal is open, and hide content
  // outside the modal from screen readers.
  usePreventScroll();
  const { modalProps } = useModal();

  const { dialogProps, titleProps } = useDialog(props, ref);

  return (
    <div className="underlay">
      <FocusScope contain restoreFocus autoFocus>
        <div
          {...overlayProps}
          {...dialogProps}
          {...modalProps}
          ref={ref}
          className={modalClassNames}
        >
          <p {...titleProps} className="modal-title">
            {title}
          </p>
          {children}
        </div>
      </FocusScope>
    </div>
  );
}

export { Modal };
