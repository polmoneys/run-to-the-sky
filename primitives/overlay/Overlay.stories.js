import React from "react";
import { Overlay } from "./";
import { IcoCircle } from "@/primitives/icon";
import { Separator, Spacer } from "@/primitives/box";
import { Text } from "@/primitives/text";
import { Button } from "@/primitives/action";
import { useButton } from "@react-aria/button";
import { useBoolean } from "@/utils/use-boolean";
import { DismissButton } from "@react-aria/overlays";
export const modal = ({ title = "ceci n'est pas une modal" }) => {
  const [status, toggle] = useBoolean(false);

  return (
    <>
      <Button onPress={() => toggle()}>Open Modal</Button>
      <Spacer />
      {status && (
        <Overlay.Modal
          isOpen={status}
          isDismissable
          onClose={toggle}
          title={title}
        >
          <Separator />
          <Text.LoremXL />
          <DismissButton onDismiss={() => toggle()} />
        </Overlay.Modal>
      )}
    </>
  );
};

export const tray = ({ title = "ceci n'est pas une tray" }) => {
  const [status, toggle] = useBoolean(false);
  return (
    <>
      <Button onPress={() => toggle()}>Open Tray</Button>
      <Spacer />
      {status && (
        <Overlay.Tray
          isOpen={status}
          isDismissable
          onClose={toggle}
          title={title}
        >
          <Separator />
          <Text.LoremXL />
          <DismissButton onDismiss={() => toggle()} />
        </Overlay.Tray>
      )}
    </>
  );
};

export const toast = ({ title = "ceci n'est pas une toast" }) => {
  const [status, toggle] = useBoolean(false);

  return (
    <>
      <Button onPress={() => toggle()}>Open Toasts</Button>
      <Spacer />
      {status && (
        <Overlay.Toast
          isOpen={status}
          isDismissable
          onClose={toggle}
          title={title}
        >
          <DismissButton onDismiss={() => toggle()} />
        </Overlay.Toast>
      )}
    </>
  );
};
