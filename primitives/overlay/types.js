import PropTypes from "prop-types";

const PROPTYPES = {
  title: PropTypes.string.isRequired,
  variant: PropTypes.string,
  children: PropTypes.any.isRequired,
  metaClassName: PropTypes.string,
  className: PropTypes.string,
};

const DEFAULT_PROPTYPES = {
  title: undefined,
  variant: "modal",
  metaClassName: undefined,
  className: undefined,
  children: undefined,
};

export { PROPTYPES, DEFAULT_PROPTYPES };
