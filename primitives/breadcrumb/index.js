import React, { Children, cloneElement, useRef } from "react";
import { useBreadcrumbs, useBreadcrumbItem } from "@react-aria/breadcrumbs";
import { IcoChevron } from "@/primitives/icon";

const BreadCrumbs = (props) => {
  const { navProps } = useBreadcrumbs(props);
  const items = Children.toArray(props.children);

  return (
    <nav className="breadcrumbs" {...navProps}>
      <ol className="flex">
        {items.map((child, i) =>
          cloneElement(child, { isCurrent: i === items.length - 1 }),
        )}
      </ol>
    </nav>
  );
};

const BreadCrumbsItem = (props) => {
  const ref = useRef();
  const { itemProps } = useBreadcrumbItem(
    { ...props, elementType: "span" },
    ref,
  );
  return (
    <li className="flex main-between cross-center">
      <span
        {...itemProps}
        ref={ref}
        className={
          props.isCurrent ? "is-current breadcrumb-item" : "breadcrumb-item"
        }
      >
        {props.children}
      </span>
      {!props.isCurrent && (
        <IcoChevron
          size={28}
          viewbox="-2 -4 28 28"
          transforms={"rotate(-90deg)"}
        />
      )}
    </li>
  );
};

export { BreadCrumbs, BreadCrumbsItem };
