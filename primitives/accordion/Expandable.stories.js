import React, { useEffect } from "react";
import { Meta, Story, Source, Canvas } from "@storybook/addon-docs/blocks";
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionContent,
  useAccordionItemContext,
  useAccordionContext,
} from "./";
import { Grid, Spacer, Separator, Row, Col } from "@/primitives/box";
import { Text } from "@/primitives/text";
import { Font } from "@/primitives/text/font";
import { IcoMore, IcoRemove, IcoChevron } from "@/primitives/icon";
import { Flag } from "@/primitives/icon/flag";
import { Button } from "@/primitives/action";
import { VisuallyHidden } from "@react-aria/visually-hidden";

const RunnerCardHeader = ({
  finalTime = "46 : 24 : 04",
  name = "Ruth Charlotte Croft",
  points = 10000,
  nationality = "https://i.imgur.com/zY5yClC.png",
}) => {
  const { id } = useAccordionContext();
  const { isExpanded } = useAccordionItemContext();
  return (
    <Row>
      <Col>
        <Font.FuturaBook16>{points}</Font.FuturaBook16>
        {!isExpanded && <Flag flag={nationality} />}
      </Col>
      <Col className="self is-large">
        {!isExpanded && <Font.FuturaMedium16>{finalTime}</Font.FuturaMedium16>}
        <Font.FuturaMedium20>{name}</Font.FuturaMedium20>
      </Col>
      <AccordionButton
        style={{
          appearance: "none",
          background: 0,
          border: 0,
          boxShadow: "none",
          color: "inherit",
          textAlign: "inherit",
        }}
      >
        <VisuallyHidden>Expand info</VisuallyHidden>
        <IcoMore />
      </AccordionButton>
    </Row>
  );
};

const CardRow = (props) => (
  <Row {...props} className="main-between cross-center" />
);

const RunnerCard = ({ nationality = "https://i.imgur.com/zY5yClC.png" }) => {
  const { id } = useAccordionContext();
  const { isExpanded } = useAccordionItemContext();

  return (
    <AccordionItem>
      <RunnerCardHeader />
      <AccordionContent>
        <Col>
          <Row className="self-stretch">
            <Flag flag={nationality} />
          </Row>
          <CardRow>
            <Font.FuturaMedium16>BIB</Font.FuturaMedium16>
            <Font.FuturaBook16>4753</Font.FuturaBook16>
          </CardRow>
          <CardRow>
            <Font.FuturaMedium16>BIB</Font.FuturaMedium16>
            <Font.FuturaBook16>4753</Font.FuturaBook16>
          </CardRow>
          <CardRow>
            <Font.FuturaMedium16>BIB</Font.FuturaMedium16>
            <Font.FuturaBook16>4753</Font.FuturaBook16>
          </CardRow>
          <CardRow>
            <Button.Ghost>
              RUNNER <IcoChevron size={14} transforms={"rotate(90deg)"} />
            </Button.Ghost>
            <Button.Ghost>
              LIVE <IcoChevron size={14} transforms={"rotate(90deg)"} />
            </Button.Ghost>
          </CardRow>
        </Col>
      </AccordionContent>
    </AccordionItem>
  );
};

export const ExpandableCard = () => (
  <>
    <h1>An expandable card</h1>
    <Spacer />
    <Accordion className="accordion-card">
      <RunnerCard />
    </Accordion>
  </>
);

export const CardsList = ({ of = [1, 2, 3, 4, 5, 6, 7, 8] }) => (
  <>
    <h1>A list of cards</h1>
    <Spacer />
    <Grid className="grid-gap">
      {of.map((i) => (
        <Accordion className="accordion-card" key={i}>
          <RunnerCard />
        </Accordion>
      ))}
    </Grid>
  </>
);

export const ExpandableMultiple = () => (
  <>
    <h1>An accordion with sub accordions</h1>
    <Accordion>
      <AccordionItem>
        <h3>
          <AccordionButton>Step 1: Do a thing</AccordionButton>
        </h3>
        <AccordionContent>
          <Text.LoremXL />
        </AccordionContent>
      </AccordionItem>
      <AccordionItem>
        <h3>
          <AccordionButton>Step 2: Do another thing</AccordionButton>
        </h3>
        <AccordionContent>
          <Text.LoremXL />
        </AccordionContent>
        <Spacer />
        <Separator />
        <Text.Lorem />
        <Separator />
        <Spacer />
      </AccordionItem>
      <AccordionItem>
        <h3>
          <AccordionButton>Step 3: Do another thing</AccordionButton>
        </h3>
        <AccordionContent>
          <Text.LoremXL />
        </AccordionContent>
      </AccordionItem>
    </Accordion>
  </>
);
