import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  useAccordionContext,
  useAccordionItemContext,
} from "@reach/accordion";
import React, { useState } from "react";

const _Accordion = ({ children, collapsible = true, ...rest }) => {
  // const [index, setIndex] = useState()
  return (
    <Accordion
      collapsible={collapsible}
      //   index={index} onChange={(value) => setIndex(value)}
      {...rest}
    >
      {children}
    </Accordion>
  );
};

export {
  _Accordion as Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel as AccordionContent,
  useAccordionItemContext,
  useAccordionContext,
};
