import React from "react";
import { useTable } from "./use-table";
import { VisuallyHidden } from "@react-aria/visually-hidden";
import { IcoChevron } from "@/primitives/icon";
import { Button } from "@/primitives/action";

const getValueByColumnKey = (o, key) => o[key];

const Table = ({
  caption,
  of,
  columnKeys,
  toolbar,
  header,
  children,
  predicates,
  ops,
  className = undefined,
  ...rest
}) => {
  const tableDatum = of.map((v) =>
    Object.keys(v)
      .filter((k) => columnKeys.includes(k))
      .reduce((acc, key) => ((acc[key] = v[key]), acc), {}),
  );
  const [tableState, actions] = useTable({
    xs: tableDatum,
    columnKeys,
    predicates,
    ops,
    ...rest,
  });

  const { xs, ready, activeFilters, sortKey, sortDirection } = tableState;
  const { sort } = actions;

  if (!ready) return null; // TODO: Placeholder or skeleton
  return (
    <>
      {toolbar &&
        toolbar({
          columnKeys,
          actions,
          activeFilters,
          sortKey,
          sortDirection,
        })}
      <table className={className}>
        <VisuallyHidden elementType="caption">{caption}</VisuallyHidden>
        <thead>
          <tr>
            {header && header({ columnKeys, sort, sortKey, sortDirection })}
          </tr>
        </thead>
        <tbody>
          {children && children({ columnKeys, xs, getValueByColumnKey })}
        </tbody>
      </table>
    </>
  );
};

export { Table };

// TODO: React.memo() <TableItem/>

const isSorting = ({ sortKey, columnKey }) => sortKey === columnKey;

const TableHeaderItem = (props) => {
  const {
    variant = "",
    children,
    sortKey,
    onPress = () => ({}),
    columnKey,
    sortDirection,
    ...rest
  } = props;

  if (variant === "")
    return (
      <th scope="col" {...rest}>
        {children}
      </th>
    );

  const chevron = !isSorting(props) ? (
    <IcoChevron size={28} viewbox="-2 -4 28 28" />
  ) : isSorting(props) && sortDirection === "isAsc" ? (
    <IcoChevron size={28} viewbox="-2 -4 28 28" />
  ) : (
    <IcoChevron
      size={28}
      viewbox="-2 -4 28 28"
      transforms={"rotate(180deg) translate(-2px)"}
    />
  );
  return (
    <th
      scope="col"
      aria-label={`sort ${columnKey}`}
      {...rest}
      style={{
        opacity: isSorting(props) ? 1 : 0.8,
      }}
    >
      <Button.Ghost onPress={() => onPress()}>
        {children}
        {chevron}
      </Button.Ghost>
    </th>
  );
};

TableHeaderItem.Sortable = (props) => (
  <TableHeaderItem variant="sort" {...props} />
);

const TableRowItem = (props) => <tr {...props}>{props.children}</tr>;
const TableCellItem = (props) => <td {...props}>{props.children}</td>;

export { TableRowItem, TableCellItem, TableHeaderItem };
