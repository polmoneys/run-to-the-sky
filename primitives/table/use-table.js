import { useEffect, useReducer, useCallback, useRef } from "react";
import { reducer } from "./use-table-reducer";

function useTable(initialState) {
  const initialRef = useRef(initialState.xs);
  const [state, dispatch] = useReducer(reducer, initialState);

  const filter = useCallback((predicate) => {
    let match = [];
    let dispose = [];
    for (const el of initialRef.current) {
      if (predicate(el) === true) {
        match.push(el);
      } else {
        dispose.push(el);
      }
    }
    return match;
  }, []);

  useEffect(() => {
    if (state.ready) return;

    let b = {};

    for (const bb of initialState.ops) {
      b = {
        ...b,
        [bb]: filter(initialState.predicates[bb]),
      };
    }

    const payload = {
      xs: initialRef.current,
      buckets: b,
      initial: initialRef.current,
      activeFilters: [],
      sortDirection: "",
      sortKey: "",
    };

    dispatch({ type: useTable.types.onload, payload });
  }, [initialRef, filter, state]);

  async function chunk(k) {
    let upcomingFilters;
    if (state.activeFilters.includes(k)) {
      upcomingFilters = state.activeFilters.filter((f) => f !== k);
    } else {
      upcomingFilters = state.activeFilters.concat(k);
    }

    let upcomingActive = [];
    for (const r of upcomingFilters) {
      let column = state.buckets[r];
      upcomingActive.push(...column);
    }

    const uniq = Array.from(
      new Set(upcomingActive.map((x) => JSON.stringify(x))),
    ).map((x) => JSON.parse(x));

    const payload = {
      xs: upcomingActive.length > 0 ? uniq : initialRef.current,
      activeFilters: upcomingFilters,
    };

    dispatch({ type: useTable.types.chunk, payload });
  }

  function sort(k) {
    if (!k) return;
    const direction = state.sortDirection === "isDesc" ? "isAsc" : "isDesc";
    const sorted = state.xs.sort((a, b) => {
      return (a[k] < b[k] ? -1 : 1) * (direction === "isAsc" ? 1 : -1);
    });
    const payload = {
      xs: sorted,
      sortDirection: direction,
      sortKey: k,
    };
    dispatch({ type: useTable.types.sort, payload });
  }

  function undo() {
    dispatch({ type: useTable.types.flush });
  }

  const actions = {
    chunk,
    sort,
    undo,
  };

  return [state, actions];
}

export { useTable };
export const getValueByColumnKey = (o, key) => o[key];

useTable.types = {
  chunk: "CHUNK",
  unchunk: "UNCHUNK",
  sort: "SORT",
  onload: "BOOTSTRAP",
  flush: "FLUSH",
};
