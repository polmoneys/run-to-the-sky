import { useTable } from "./use-table";

function reducer(state, action) {
  switch (action.type) {
    case useTable.types.onload: {
      const { payload } = action;
      return {
        ...payload,
        ready: true,
      };
    }

    case useTable.types.chunk: {
      const { payload } = action;
      return {
        ...state,
        ...payload,
      };
    }

    case useTable.types.sort: {
      const { payload } = action;
      return {
        ...state,
        ...payload,
      };
    }

    case useTable.types.flush: {
      return {
        ...state,
        xs: state.initial,
        activeFilters: [],
        sortKey: "",
        sortDirection: "",
      };
    }

    default: {
      throw new Error(`Unhandled type: ${action.type}`);
    }
  }
}

export { reducer };
