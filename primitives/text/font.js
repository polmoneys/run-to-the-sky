import React, { useRef } from "react";

const Font = (props) => {
  const {
    as = "p",
    className = undefined,
    composeClassName = undefined,
    children,
  } = props;
  const Tag = as;
  const tagClassNames = [composeClassName, className].filter(Boolean).join(" ");
  return <Tag className={tagClassNames}>{children}</Tag>;
};

const addLetterSpacing = (Element) => (props) => (
  <Element {...props} className="letter-spacing-0-78px" />
);

const addMixBlendFont = (Element) => (props) => (
  <Element {...props} className="card-title-transparent" />
);

Font.FuturaLight36 = (props) => (
  <Font {...props} composeClassName="font-futura is-light is-36" />
);
Font.FuturaMedium16 = (props) => (
  <Font {...props} composeClassName="font-futura is-medium is-16" />
);
Font.FuturaMedium20 = (props) => (
  <Font {...props} composeClassName="font-futura is-medium is-20" />
);
Font.FuturaMedium32 = (props) => (
  <Font {...props} composeClassName="font-futura is-medium is-32" />
);
Font.FuturaBook14 = (props) => (
  <Font {...props} composeClassName="font-futura is-book is-14" />
);
Font.FuturaBook16 = (props) => (
  <Font {...props} composeClassName="font-futura is-book is-16" />
);
Font.FuturaBook18 = (props) => (
  <Font {...props} composeClassName="font-futura is-book is-18" />
);
Font.FuturaBook20 = (props) => (
  <Font {...props} composeClassName="font-futura is-book is-20" />
);
Font.FuturaBook32 = (props) => (
  <Font {...props} composeClassName="font-futura is-book is-32" />
);
Font.FuturaHeavy16 = (props) => (
  <Font {...props} composeClassName="font-futura is-heavy is-16" />
);

Font.SpaceRegular14 = (props) => (
  <Font {...props} composeClassName="font-space-mono is-regular is-14" />
);
Font.SpaceBook18 = (props) => (
  <Font {...props} composeClassName="font-space-mono is-book is-18" />
);
Font.SpaceBold18 = (props) => (
  <Font {...props} composeClassName="font-space-mono is-bold is-18" />
);
Font.SpaceBold16 = (props) => (
  <Font {...props} composeClassName="font-space-mono is-bold is-16" />
);
Font.SpaceBold24 = (props) => (
  <Font {...props} composeClassName="font-space-mono is-bold is-24" />
);

export { Font, addLetterSpacing };
