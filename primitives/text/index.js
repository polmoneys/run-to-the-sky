import React, { useRef } from "react";
import { AutocopyText } from "./copy-to-clipboard";
import { Stat, Money, Distance, Speed, ProgressPercent } from "./number";
import Truncate from "./truncate";
import { operations, Lorem, LoremXL } from "./utils";
import { PROPTYPES, DEFAULT_PROPTYPES } from "./types";

function Text(props) {
  const { as, op, className, composeClassName, ...rest } = props;
  const classNames = [className, composeClassName].filter(Boolean).join(" ");
  let output = useRef(null);
  output.current = operations[op](props);
  const Tag = as;
  return (
    <Tag className={classNames} {...rest}>
      {output && output.current}
    </Tag>
  );
}

Text.Cta = (props) => {
  const Tag = props.as;
  return <Tag className="is-cta">{props.children}</Tag>;
};

Text.Copy = (props) => <AutocopyText {...props} />;
Text.Truncate = (props) => <Truncate {...props} />;

// LOGIC AT ./utils.js

Text.Caps = (props) => <Text op="caps" {...props} />;
Text.Shout = (props) => <Text op="shout" {...props} />;

// FORMATTED AT ./numbers.js

Text.Money = (props) => <Money {...props} />;
Text.Speed = (props) => <Speed {...props} />;
Text.Distance = (props) => <Distance {...props} />;
Text.Stat = (props) => <Stat {...props} />;
Text.ProgressPercent = (props) => <ProgressPercent {...props} />;

/* eslint-disable no-undef */
if (process.env.NODE_ENV === "development") {
  Text.Lorem = () => <Lorem />;
  Text.LoremXL = () => <LoremXL />;
}
/* eslint-enable */

Text.propTypes = PROPTYPES;
Text.defaultProps = DEFAULT_PROPTYPES;

export { Text };
