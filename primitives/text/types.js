import PropTypes from "prop-types";

const PROPTYPES = {
  children: PropTypes.string.isRequired,
  op: PropTypes.oneOf(["shout", "caps", "matches", "is-cta"]),
  as: PropTypes.string,
};

const DEFAULT_PROPTYPES = {
  children: " ",
  op: "",
  as: "span",
};

export { PROPTYPES, DEFAULT_PROPTYPES };
