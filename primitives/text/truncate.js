import React, { useState } from "react";
import { Button } from "@/primitives/action";

const Truncate = (props) => {
  let { text, maxLength } = props;
  const [hidden, setHidden] = useState(true);
  if (text <= maxLength) {
    return <span>{text}</span>;
  }

  return (
    <span>
      {hidden ? `${text.substr(0, maxLength).trim()} ...` : text}
      {hidden ? (
        <Button.Ghost onPress={() => setHidden(false)}>read more</Button.Ghost>
      ) : (
        <Button.Ghost onPress={() => setHidden(true)}>read less</Button.Ghost>
      )}
    </span>
  );
};

export default Truncate;
