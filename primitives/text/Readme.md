# What

`<Text>`

The goal is to avoid composing classNames in `units/` or `page.js` and do some operations on strings for you like internationalization.

Think of it as an infinte umbrella that gives you stuff like:

# Api

```javascript
Text.Copy = (props) => <AutocopyText {...props} />;
Text.Truncate = (props) => <Truncate {...props} />;

// ADD OP AT ./utils.js

Text.Caps = (props) => <Text op="caps" {...props} />;
Text.Shout = (props) => <Text op="shout" {...props} />;
Text.MatchStart = (props) => <Text op="matches" {...props} />;

// ADD FORMATS AT ./numbers.js

Text.Money = (props) => <Money {...props} />;
Text.Speed = (props) => <Speed {...props} />;
Text.Distance = (props) => <Distance {...props} />;
Text.Stat = (props) => <Stat {...props} />;
Text.ProgressPercent = (props) => <ProgressPercent {...props} />;

/* eslint-disable no-undef */
if (process.env.NODE_ENV === "development") {
  Text.Lorem = () => <Lorem />;
  Text.LoremXL = () => <LoremXL />;
}
/* eslint-enable */
```

Also, it gives aliases of most used font combos:

```javascript

// USAGE : <Font.FuturaLight36>A beautiful string </Font.FuturaLight36>

<Font.FuturaLight36  />
<Font.FuturaMedium16 />
<Font.FuturaMedium20 />
<Font.FuturaMedium32 />
<Font.FuturaBook16 />
<Font.FuturaBook18 />
<Font.FuturaBook20 />
<Font.FuturaBook32 />
<Font.FuturaHeavy16  />

<Font.SpaceRegular14 />
<Font.SpaceBook18 />
<Font.SpaceBold18 />
<Font.SpaceBold24  />

```
