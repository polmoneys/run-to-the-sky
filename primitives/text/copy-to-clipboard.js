import React, { useState, useEffect } from "react";
import { Button } from "@/primitives/action";

function AutocopyText(props) {
  const { children, className = undefined, tag = "p" } = props;
  const [supported, setStatus] = useState(false);
  const [ok, setOk] = useState(false);
  const [fail, setFail] = useState(false);

  useEffect(() => {
    if (
      navigator.clipboard &&
      navigator.clipboard.read &&
      navigator.clipboard.write
    ) {
      setStatus(true);
    }
  }, [setStatus]);

  const tryCopy = async (event) => {
    event.preventDefault();
    try {
      await navigator.clipboard.writeText(children);
      console.log(children);
      setOk(true);
    } catch (err) {
      console.log("Go to full screen for it to work on codesandbox", err);
      setFail(true);
    }
  };

  const Tag = tag;
  return (
    <>
      <Tag className={className}>{children}</Tag>{" "}
      {supported && !fail && (
        <Button.Ghost
          className={ok ? "is-accent-theme" : undefined}
          onClick={tryCopy}
        >
          {ok ? "COPIED" : "COPY ID NUMBER"}
        </Button.Ghost>
      )}
    </>
  );
}

export { AutocopyText };
