import React, { useRef } from "react";

import { useNumberFormatter } from "@react-aria/i18n";

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat/NumberFormat
// https://tc39.es/proposal-unified-intl-numberformat/section6/locales-currencies-tz_proposed_out.html#sec-issanctionedsimpleunitidentifier

function Stat({ value, currency }) {
  const formatter = useNumberFormatter({
    notation: "compact",
    compactDisplay: "long",
  });

  return <p>{formatter.format(value)}</p>;
}

function Money({ value, currency = "EUR" }) {
  const formatter = useNumberFormatter({
    style: "currency",
    currency,
    minimumFractionDigits: 0,
  });

  return <p>{formatter.format(value)}</p>;
}

function Distance({ value }) {
  const formatter = useNumberFormatter({
    style: "unit",
    unit: "kilometer", //
    minimumIntegerDigits: 2,
    unitDisplay: "long",
  });

  return <p>{formatter.format(value)}</p>;
}

function Speed({ value }) {
  const formatter = useNumberFormatter({
    style: "unit",
    unit: "kilometer-per-hour",
    minimumIntegerDigits: 2,
  });

  return <p>{formatter.format(value)}</p>;
}

function ProgressPercent({ value }) {
  const formatter = useNumberFormatter({
    style: "percent",
    signDisplay: "exceptZero",
  });

  return <p>{formatter.format(value)}</p>;
}

export { Stat, Money, Distance, Speed, ProgressPercent };
