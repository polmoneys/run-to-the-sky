import React from "react";

const operations = {
  caps: (props) => capitalizeSentence(props.children),
  shout: (props) => uppercase(props.children),
};

export { operations };

const Right = (x) => ({
  chain: (f) => f(x),
  map: (f) => Right(f(x)),
  fold: (f, g) => g(x),
  inspect: () => `Right(${x})`,
  concat: (o) =>
    o.fold(
      (e) => Left(e),
      (r) => Right(x.concat(r)),
    ), // to do
});

const Left = (x) => ({
  chain: (f) => Left(x),
  map: (f) => Left(x),
  fold: (f, g) => f(x),
  inspect: () => `Left(${x})`,
  concat: (o) => Left(x), // to do
});

const fromNullableString = (x) =>
  x != null && Boolean(x.trim()) ? Right(x) : Left(null);

const ERROR_STRING = "Can not do transformation ";

const capitalizeSentenceRegexp = /(:?\.\s?|^)([A-Za-z\u00C0-\u1FFF\u2800-\uFFFD])/gi; // n.charAt(0).toUpperCase() + n.slice(1)
const capitalizeSentence = (str) =>
  fromNullableString(str).fold(
    (e) => ERROR_STRING,
    (n) => n.replace(capitalizeSentenceRegexp, (match) => match.toUpperCase()),
  );

const uppercase = (str) =>
  fromNullableString(str).fold(
    (e) => ERROR_STRING,
    (n) => n.toUpperCase(),
  );

const startsWith = (str, search) =>
  fromNullableString(str).fold(
    (e) => ERROR_STRING,
    (n) => [n, n.toLowerCase().substr(0, search.length) === search],
  );

const Lorem = () => <p>Lorem ipsum dolor sit amet.</p>;
const loremMD =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt gloria est tu.";
const LoremXL = () => <p>{loremMD.repeat(5)}</p>;

export { LoremXL, Lorem };
