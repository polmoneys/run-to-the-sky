import { useState, useCallback } from "react";
import { useEmblaCarousel } from "embla-carousel/react";
import { Slide } from "./slide";
import { SlidesLazy } from "./lazy";
import { SlidesParallax } from "./paralax";
import { PrevButton, NextButton } from "./buttons";
import { PROPTYPES, DEFAULT_PROPTYPES } from "./types";

/*

    EMBLA USAGE
    -----------

    [API](https://davidcetinkaya.github.io/embla-carousel/api#options)
    
    Interesting:
    embla.reInit({ ...updateOpts })

*/

const Slides = (props) => {
  const {
    datum,
    className,
    composeClassName,
    item,
    ...carouselOptions
  } = props;
  const slideClasses = ["embla__slide", className, composeClassName]
    .filter(Boolean)
    .join(" ");

  const [viewportRef, embla] = useEmblaCarousel({ loop: false });
  const [prevBtnEnabled, setPrevBtnEnabled] = useState(false);
  const [nextBtnEnabled, setNextBtnEnabled] = useState(false);

  const scrollPrev = useCallback(() => embla && embla.scrollPrev(), [embla]);
  const scrollNext = useCallback(() => embla && embla.scrollNext(), [embla]);
  const onSelect = useCallback(() => {
    if (!embla) return;
    setPrevBtnEnabled(embla.canScrollPrev());
    setNextBtnEnabled(embla.canScrollNext());
  }, [embla]);

  useEffect(() => {
    if (!embla) return;
    embla.on("select", onSelect);
    onSelect();
  }, [embla, onSelect]);

  if (!datum || datum.length <= 0) return null;

  return (
    <div className="embla__viewport" ref={viewportRef}>
      <div className="embla__container">
        {datum.map((slide) => {
          item ? (
            item({
              className: slideClasses,
            })
          ) : (
            <Slide key={slide.id} className={slideClasses} {...slide} />
          );
        })}{" "}
        {children}
      </div>
      <PrevButton onClick={scrollPrev} enabled={prevBtnEnabled} />
      <NextButton onClick={scrollNext} enabled={nextBtnEnabled} />
    </div>
  );
};

Slides.Small = (props) => <Slides composeClassName="is-s" {...props} />;
Slides.XtraSmall = (props) => <Slides composeClassName="is-xs" {...props} />;
Slides.Lazy = (props) => <SlidesLazy {...props} />;
Slides.Parallax = (props) => <SlidesParallax {...props} />;

export { Slides };

Slides.propTypes = PROPTYPES;
Slides.defaultProps = DEFAULT_PROPTYPES;
