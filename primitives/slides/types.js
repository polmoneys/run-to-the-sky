import PropTypes from "prop-types";

const PROPTYPES = {
  item: PropTypes.bool,
  metaClassName: PropTypes.string,
  className: PropTypes.string,
  xs: PropTypes.array,
};

const DEFAULT_PROPTYPES = {
  item: false,
  metaClassName: undefined,
  className: undefined,
  xs: [],
};

export { PROPTYPES, DEFAULT_PROPTYPES };
