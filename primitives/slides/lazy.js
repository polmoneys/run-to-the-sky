import { useState, useCallback } from "react";
import { useEmblaCarousel } from "embla-carousel/react";
import { PrevButton, NextButton } from "./buttons";
import { PLACEHOLDER_SRC } from "@/tokens";

const Slides = (props) => {
  const {
    datum,
    className,
    composeClassName,
    item,
    ...carouselOptions
  } = props;
  const slideClasses = ["embla__slide", className, composeClassName]
    .filter(Boolean)
    .join(" ");

  const [slidesInView, setSlidesInView] = useState([]);
  const [viewportRef, embla] = useEmblaCarousel(carouselOptions);
  const [prevBtnEnabled, setPrevBtnEnabled] = useState(false);
  const [nextBtnEnabled, setNextBtnEnabled] = useState(false);

  const scrollPrev = useCallback(() => embla && embla.scrollPrev(), [embla]);
  const scrollNext = useCallback(() => embla && embla.scrollNext(), [embla]);
  const onSelect = useCallback(() => {
    if (!embla) return;
    setPrevBtnEnabled(embla.canScrollPrev());
    setNextBtnEnabled(embla.canScrollNext());
  }, [embla]);

  const findSlidesInView = useCallback(() => {
    if (!embla) return;

    setSlidesInView((slidesInView) => {
      if (slidesInView.length === embla.slideNodes().length) {
        embla.off("select", findSlidesInView);
      }
      const inView = embla
        .slidesInView(true)
        .filter((index) => slidesInView.indexOf(index) === -1);
      return slidesInView.concat(inView);
    });
  }, [embla, setSlidesInView]);

  useEffect(() => {
    if (!embla) return;
    onSelect();
    findSlidesInView();
    embla.on("select", onSelect);
    embla.on("select", findSlidesInView);
  }, [embla, onSelect, findSlidesInView]);

  if (!datum || datum.length <= 0) return null;

  return (
    <div className="embla__viewport" ref={viewportRef}>
      <div className="embla__container">
        {datum.map((slide, index) => {
          item ? (
            item({
              inView: slidesInView.indexOf(index) > -1,
              className: slideClasses,
              ...slide,
            })
          ) : (
            <Slide
              key={slide.id}
              inView={slidesInView.indexOf(index) > -1}
              className={slideClasses}
              {...slide}
            />
          );
        })}
      </div>
      <PrevButton onClick={scrollPrev} enabled={prevBtnEnabled} />
      <NextButton onClick={scrollNext} enabled={nextBtnEnabled} />
    </div>
  );
};

export { Slides as SlidesLazy };

const Slide = ({ title, url, inView, className }) => {
  const [hasLoaded, setHasLoaded] = useState(false);
  const slideClasses = [className, hasLoaded && "has-loaded"]
    .filter(Boolean)
    .join(" ");

  const setLoaded = useCallback(() => {
    if (inView) setHasLoaded(true);
  }, [inView, setHasLoaded]);

  return (
    <div className={slideClasses}>
      <p>{title}</p>
      <img
        src={inView ? url : PLACEHOLDER_SRC}
        onLoad={setLoaded}
        alt={title}
      />
    </div>
  );
};
