import { useState, useCallback } from "react";
import { useEmblaCarousel } from "embla-carousel/react";
import { PrevButton, NextButton } from "./buttons";

const PARALLAX_FACTOR = 1.2;

const Slides = (props) => {
  const {
    datum,
    className,
    composeClassName,
    item,
    ...carouselOptions
  } = props;
  const slideClasses = ["embla__slide", className, composeClassName]
    .filter(Boolean)
    .join(" ");

  const [viewportRef, embla] = useEmblaCarousel({
    align: "center",
    dragFree: true,
  });

  const [parallaxValues, setParallaxValues] = useState([]);

  const onScroll = useCallback(() => {
    if (!embla) return;
    const styles = embla.scrollSnapList().map((scrollSnap) => {
      const diffToTarget = scrollSnap - embla.scrollProgress();
      return diffToTarget * (-1 / PARALLAX_FACTOR) * 100;
    });
    setParallaxValues(styles);
  }, [embla, setParallaxValues]);

  if (!datum || datum.length <= 0) return null;

  return (
    <div className="embla__viewport" ref={viewportRef}>
      <div className="embla__container">
        {datum.map((slide, index) => {
          item ? (
            item({
              className: slideClasses,
              ...slide,
            })
          ) : (
            <Slide key={slide.id} className={slideClasses} {...slide} />
          );
        })}
      </div>
      <PrevButton onClick={scrollPrev} enabled={prevBtnEnabled} />
      <NextButton onClick={scrollNext} enabled={nextBtnEnabled} />
    </div>
  );
};

export { Slides as SlidesParallax };
