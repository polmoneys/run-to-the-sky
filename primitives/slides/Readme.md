# SLIDES

Wrapper of `embla-carousel` to expose a subset of the API. You should mostly configure `<Slide>`.

```javascript

<Slides/>
<Slides.Small/>
<Slides.XtraSmall/>
<Slides.Lazy/>

```

Control `<Slide>` width with `.css`, this is defaults + presets:

```css
@media (orientation: landscape) {
  .embla__slide {
    flex: 0 0 50%;
  }
  .embla__slide.is-small,
  .embla__slide.is-xtrasmall {
    flex: 0 0 33.3333%;
  }
}

@media (orientation: landscape) and (min-width: 1040px) {
  .embla__slide {
    flex: 0 0 33.3333%;
  }
  .embla__slide.is-small {
    flex: 0 0 25%;
  }
  .embla__slide.is-xtrasmall {
    flex: 0 0 20%;
  }
}
```
