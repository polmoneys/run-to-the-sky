import React, { useState } from "react";
import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";

const _Tabs = ({
  startIndex = 0,
  keyboardActivation = "manual",
  children = undefined,
  orientation = "horizontal",
  header = undefined,
}) => {
  const [tabIndex, setTabIndex] = useState(startIndex);

  const onTabsChange = (index) => {
    setTabIndex(index);
  };

  return (
    <Tabs
      orientation={orientation}
      keyboardActivation={keyboardActivation}
      index={tabIndex}
      onChange={onTabsChange}
    >
      {({ selectedIndex, focusedIndex }) => {
        return (
          <>
            <TabList>
              {header({
                selectedIndex,
                focusedIndex,
              })}
            </TabList>
            <TabPanels>{children}</TabPanels>
          </>
        );
      }}
    </Tabs>
  );
};

_Tabs.V = (props) => <_Tabs orientation="vertical" {...props} />;

const DataTabs = ({ data }) => {
  return (
    <Tabs>
      <TabList>
        {data.map((tab, index) => (
          <Tab key={index}>{tab.label}</Tab>
        ))}
      </TabList>
      <TabPanels>
        {data.map((tab, index) => (
          <TabPanel key={index}>{tab.content}</TabPanel>
        ))}
      </TabPanels>
    </Tabs>
  );
};

export { _Tabs as Tabs, DataTabs, Tab as TabHeader, TabPanel as TabItem };
