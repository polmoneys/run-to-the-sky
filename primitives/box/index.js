import React from "react";
import { BOX_PROPTYPES, BOX_DEFAULT_PROPTYPES } from "./types";
import { Pin } from "./pin";
import { BoxMQ } from "./observed";
import { Spacer, Br } from "./spacer";
import { Separator } from "./separator";
export { Br, BoxMQ, Pin, Separator, Spacer };

const Box = ({ children, ...props }) => {
  const { as, className, composeClassName, ...extra } = props;
  const Tag = as;
  const clss = [className, composeClassName].filter(Boolean).join(" ");
  return (
    <Tag className={clss} {...extra}>
      {children}
    </Tag>
  );
};

const Row = (props) => <Box composeClassName="flex" {...props} />;
const Col = (props) => <Box composeClassName="flex col" {...props} />;
const Grid = (props) => <Box composeClassName="flex grid" {...props} />;
const ColToRow = (props) => (
  <Box composeClassName="flex-landscape" {...props} />
);
const SuperBox = (props) => <Box composeClassName="flex-widget" {...props} />;

Box.propTypes = BOX_PROPTYPES;
Box.defaultProps = BOX_DEFAULT_PROPTYPES;

export { Box, Row, Col, Grid, ColToRow, SuperBox };
