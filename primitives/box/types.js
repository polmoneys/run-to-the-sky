import PropTypes from "prop-types";

const PROPTYPES = {
  as: PropTypes.string,
  children: PropTypes.any.isRequired,
  xy: PropTypes.any,
};

const DEFAULT_PROPTYPES = {
  as: "span",
  children: undefined,
  xy: undefined,
};

export { PROPTYPES, DEFAULT_PROPTYPES };

const BOX_PROPTYPES = {
  as: PropTypes.string,
  children: PropTypes.any,
  composeClassName: PropTypes.any,
};

const BOX_DEFAULT_PROPTYPES = {
  children: null,
  as: "div",
  composeClassName: undefined,
};

export { BOX_PROPTYPES, BOX_DEFAULT_PROPTYPES };

const BOX_MQ_PROPTYPES = {
  as: PropTypes.string,
  children: PropTypes.any.isRequired,
  matches: PropTypes.number,
};

const BOX_MQ_DEFAULT_PROPTYPES = {
  children: null,
  as: "div",
  matches: 960,
};

export { BOX_MQ_PROPTYPES, BOX_MQ_DEFAULT_PROPTYPES };
