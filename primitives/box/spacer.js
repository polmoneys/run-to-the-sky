import React from "react";
const SPACER_SIZES = Object.freeze({
  xs: "8px",
  s: "12px",
  default: "16px",
  l: "56px",
  xl: "99px",
});

const Spacer = ({ size = "default" }) => {
  const space = SPACER_SIZES[size];
  return (
    <div
      aria-hidden="true"
      className="flex self-grow"
      style={{ minHeight: space }}
      // vs. --spacer:`${space}px
    />
  );
};

const Br = () => <br aria-hidden="true" />;

export { Br, Spacer };
