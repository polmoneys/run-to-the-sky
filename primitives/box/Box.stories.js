import React, { useState } from "react";
import { Button } from "@/primitives/action";
import { IcoCircle } from "@/primitives/icon";
import { Text } from "@/primitives/text";
import {
  Box,
  BoxMQ,
  Row,
  Col,
  Grid,
  ColToRow,
  SuperBox,
  Pin,
  Separator,
  Spacer,
} from "./";
import { Circle, Card } from "../../stories/theme";

const Template = (args) => {
  return (
    <>
      <Spacer />
      <h1>A Box as Row width small gap</h1>
      <Spacer />
      <Row className="-light row-gap is-s">
        <Circle />
        <Circle />
        <Circle className="self-push-left" />
        <Circle />
      </Row>
      <Spacer />
      <h1>A Box as Col with gap</h1>
      <Spacer />
      <Col className="-light cross-center col-gap is-xl">
        <Circle />
        <Circle />
        <Circle />
        <Circle />
      </Col>

      <Spacer />
      <h1>A Grid</h1>
      <Spacer />

      <Grid className="main-center col-gap row-gap is-xl">
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
      </Grid>

      <Spacer />
      <h1>A ColToRow (portrait/landscape)</h1>
      <Spacer />

      <ColToRow className="-dark row-gap col-gap is-s">
        <Circle />
        <Circle />
        <Circle />
        <Circle />
      </ColToRow>
      <Spacer />

      <h1>A size-aware Box </h1>
      <Spacer />

      <BoxMQ className="-light" matches={500}>
        {({ isLandscape }) => (
          <>
            <h2>Lorem ipsun</h2>
            <p className={isLandscape ? "-accent" : "-dark"}>
              Matches MQ: {isLandscape ? "yay" : "nope"}
            </p>
          </>
        )}
      </BoxMQ>
      <Spacer />
      <h1>A flex widget </h1>
      <Spacer />

      <SuperBox className="is-featured">
        <h2> Title long </h2>
        <Text.LoremXL />
        <Spacer />

        <div style={{ "--direction": "row" }}>
          <div className="-accent">
            <p> 1</p>
          </div>
          <div className="is-flex-l -light">
            <p> 2</p>
          </div>
          <div className="is-flex-xl -dark">
            <p> 3</p>
          </div>
        </div>
        <Spacer />
      </SuperBox>
      <Spacer />
    </>
  );
};

export const BoxStory = Template.bind({});
