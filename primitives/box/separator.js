import React from "react";
import { useSeparator } from "@react-aria/separator";

const isVertical = (orientation) => orientation === "vertical";

const Separator = (props) => {
  const { separatorProps } = useSeparator(props);
  const { orientation = "", variant = "" } = props;
  const {
    size = "2px",
    fill = "currentColor",
    spacing = "5px",
    ...rest
  } = props;
  if (variant === "min")
    return <div {...separatorProps} className="separator-min" />;
  return (
    <div
      {...separatorProps}
      style={{
        background: fill,
        width: isVertical(orientation) ? size : "100%",
        height: isVertical(orientation) ? "100%" : size,
        minHeight: isVertical(orientation) ? "20px" : size,
        margin: isVertical(orientation) ? `0 ${spacing}` : `${spacing} 0 `,
      }}
    />
  );
};

Separator.Min = () => {
  return <Separator variant="min" />;
};

Separator.Y = (props) => {
  return <Separator {...props} orientation="vertical" />;
};

export { Separator };
