import React from "react";
import { PROPTYPES, DEFAULT_PROPTYPES } from "./types";

const Pin = ({ as, children, composeClassName, className, xy, ...aria }) => {
  const Tag = as;
  const clss = ["is-absolute", composeClassName, className]
    .filter(Boolean)
    .join(" ");

  return (
    <Tag {...aria} className={clss} style={xy}>
      {children}
    </Tag>
  );
};

export { Pin };

Pin.XY = (props) => <Pin composeClassName="is-absolute-center" {...props} />;

Pin.propTypes = PROPTYPES;
Pin.defaultProps = DEFAULT_PROPTYPES;
