## What

`<Box>`

One good solution to layout mgmt. By using some aliases of a box we can pre-apply css utility classes.

## Api

`<Row> <Col> <Grid> <ColToRow>`

Needs and accept **any** children. It will forward any attributes (id, aria-\*...) you provide. Css class management, nothing fancy.

| Prop name | Accepts | Default |
| :-------- | :-----: | ------: |
| tag       | string  |   "div" |

```javascript
const Row = (props) => <Box composeClassName="flex" {...props} />;
const Col = (props) => <Box composeClassName="flex col" {...props} />;
const Grid = (props) => <Box composeClassName="flex grid" {...props} />;
const ColToRow = (props) => (
  <Box composeClassName="flex-landscape" {...props} />
);
const SuperBox = (props) => <Box composeClassName="flex-widget" {...props} />;
```

`<BoxMQ>`

Render props to expose **isLandscape**, it will be true if it's width matches, well...**matches** prop.

| Prop name | Accepts | Default |
| :-------- | :-----: | ------: |
| matches   | number  |     960 |
| tag       | string  |   "div" |

`<SuperBox>`

I'm exploring the idea of using a new reset but it's too funky (for now) to use everywhere so I'm testing it on isolated components.

The funky thing:

```css
*,
*::before,
*::after {
  box-sizing: border-box;
  color: currentColor;
}

* {
  --direction: column;
  background-repeat: no-repeat;
  padding: 0;
  margin: 0;
  display: flex;
  flex-direction: var(--direction);
  flex: 1;
}

head,
style,
script {
  display: none;
}
```

Current testing it as:

```css
.flex-widget,
.flex-widget * {
  --direction: column;
  display: flex;
  flex-direction: var(--direction);
  flex: 1;
}
```

Usage inline:

```javascript
<div style={{ "--direction": "row" }}>// we just changed defaults</div>
```

## ENCHANCEMENTS

When css grid becomes 99% supported I expect some changes on `<Grid>`

```javascript
/**
 * 
    utility to spread styles to both parent and child
    after checking support for grid 
 * 
 */

const spreadParentStyles = ({
  w = "100%",
  h = "55px",
  gap = "12px",
  columns = 4,
}) => {
  const flexStyles = {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexWrap: "wrap",
    width: "100%",
    maxWidth: w,
    minHeight: h,
  };
  const gridStyles = {
    width: "100%",
    display: "grid",
    maxWidth: w,
    minHeight: h,
    gridColumnGap: gap,
    gridRowGap: gap,
    gridTemplateColumns: `repeat(${columns}, 1fr)`,
  };
  const gridSupport = window.CSS && CSS.supports("display:grid");
  return gridSupport ? gridStyles : flexStyles;
};

const spreadChildrenStyles = ({ percentage = "50%" }) => {
  const flexStyles = {
    width: percentage,
  };
  const gridSupport = window.CSS && CSS.supports("display:grid");
  return !gridSupport && flexStyles;
};

export { spreadChildrenStyles, spreadParentStyles };
```
