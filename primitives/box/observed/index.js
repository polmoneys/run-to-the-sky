import React, { useRef, useState, useEffect } from "react";
import { useResizeObserver } from "@/utils/use-resize-observer";
import { BOX_MQ_PROPTYPES, BOX_MQ_DEFAULT_PROPTYPES } from "../types";

const BoxMQ = ({ children, ...props }) => {
  const { as, matches, ...forwardProps } = props;

  const boxRef = useRef(null);
  const { width } = useResizeObserver(boxRef);

  const [isLandscape, setOrientation] = useState(true);

  useEffect(() => {
    if (!width || width === 0) return;
    if (width >= matches) {
      setOrientation(true);
    } else {
      setOrientation(false);
    }
  }, [width, matches]);

  const Tag = as;

  return (
    <Tag {...forwardProps} ref={boxRef}>
      {children && children({ isLandscape })}
    </Tag>
  );
};

BoxMQ.propTypes = BOX_MQ_PROPTYPES;
BoxMQ.defaultProps = BOX_MQ_DEFAULT_PROPTYPES;

export { BoxMQ };
