import React from "react";

// TODO: IF SOURCES FAIL AKA. LOAD EVENT FAILS SHOW SPECIAL POSTER ¿?

const Video = (props) => {
  const {
    defaultUrl = IMAGE_PLACEHOLDER,
    sources = {},
    alt = " ",
    controls = true,
    poster = false,
    ...elementProps
  } = props;

  return (
    <video {...elementProps} controls poster controlsList="nodownload">
      {Object.keys(sources).map((key) => (
        <source type={`image/${key}`} srcset={sources[key]} />
      ))}
    </video>
  );
};

const videoPortraitWidth = 240;
const videoPortraitHeight = videoPortraitWidth * 1.77;

Video.Portrait = (props) => (
  <Video {...props} width={videoPortraitWidth} height={videoPortraitHeight} />
);

export { Video };
