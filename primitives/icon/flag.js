import React from "react";

const Flag = ({ flag = "https://i.imgur.com/zY5yClC.png" }) => {
  return <img className="pic is-flag" src={flag} />;
};

export { Flag };
