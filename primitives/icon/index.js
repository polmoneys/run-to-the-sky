import React from "react";
import PropTypes from "prop-types";
import { Spinner } from "./spinner";

import { toolbar, shapes, crud, shapesOutline } from "./icons";

const variants = {
  ...toolbar,
  ...shapes,
  ...shapesOutline,
  // ...shop,
  // ...arrows,
  ...crud,
};

const Icon = ({
  color = "currentColor",
  transforms,
  badge,
  badgeColor = "tomato",
  debug = false,
  ...customProps
}) => {
  const styles = {
    svg: {
      verticalAlign: "middle",
      transform: transforms ? transforms : "none",
      overflow: badge ? "visible" : "hidden",
      border: debug ? "1px solid" : undefined,
    },
  };

  const { viewbox, variant, theme } = customProps;
  const pathString = variants[variant];

  return (
    <svg
      style={styles.svg}
      width={customProps.size}
      height={customProps.size}
      viewBox={viewbox}
      focusable="false"
      aria-hidden="true"
    >
      {badge && <circle fill={badgeColor} cx={7.5} cy={7.5} r={12} />}

      {theme === "outline" ? (
        <path stroke={color} strokeWidth={1} fill="none" d={pathString} />
      ) : (
        <path fill={color} d={pathString} stroke={color} strokeWidth={1} />
      )}
    </svg>
  );
};

Icon.propTypes = {
  variant: PropTypes.string.isRequired,
  size: PropTypes.number,
  color: PropTypes.string,
  badgeColor: PropTypes.string,
  transforms: PropTypes.any,
};

Icon.defaultProps = {
  size: 56,
  viewbox: "-1.4 -1.3 18 18",
  badgeColor: "tomato",
  color: "currentColor",
  transforms: undefined,
};

export { Icon };

export const IcoChevron = (props) => <Icon variant="chevron" {...props} />;

export const IcoHexagon = (props) => <Icon variant="hexagon" {...props} />;
export const IcoHexagonO = (props) => (
  <Icon variant="hexagonO" theme="outline" {...props} />
);
export const IcoHeart = (props) => <Icon variant="heart" {...props} />;
export const IcoHeartO = (props) => (
  <Icon variant="heartO" theme="outline" {...props} />
);
export const IcoCircle = (props) => <Icon variant="circle" {...props} />;
export const IcoCircleO = (props) => (
  <Icon variant="circleO" theme="outline" {...props} />
);
export const IcoSquare = (props) => <Icon variant="square" {...props} />;
export const IcoSquareO = (props) => (
  <Icon variant="squareO" theme="outline" {...props} />
);
export const IcoHome = (props) => <Icon variant="home" {...props} />;
export const IcoHomeO = (props) => (
  <Icon variant="homeO" theme="outline" {...props} />
);
export const IcoStar = (props) => <Icon variant="star" {...props} />;
export const IcoStarO = (props) => (
  <Icon variant="starO" theme="outline" {...props} />
);
export const IcoTriangle = (props) => <Icon variant="triangle" {...props} />;
export const IcoTriangleO = (props) => (
  <Icon variant="triangleO" theme="outline" {...props} />
);
export const IcoRemove = (props) => <Icon variant="remove" {...props} />;

export const IcoSpinner = (props) => <Spinner {...props} />;

export const IcoBold = (props) => (
  <Icon variant="bold" theme="outline" {...props} />
);
export const IcoItalic = (props) => (
  <Icon variant="italic" theme="outline" {...props} />
);
export const IcoParagraph = (props) => (
  <Icon variant="paragraph" theme="outline" {...props} />
);
export const IcoText = (props) => (
  <Icon variant="text" theme="outline" {...props} />
);
export const IcoUnderline = (props) => (
  <Icon variant="underline" theme="outline" {...props} />
);
export const IcoMore = (props) => (
  <Icon variant="more" theme="outline" {...props} />
);
export const IcoMoreV = (props) => (
  <Icon variant="morevertical" theme="outline" {...props} />
);
export const IcoHashtag = (props) => (
  <Icon variant="hashtag" theme="outline" {...props} />
);
