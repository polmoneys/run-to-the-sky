import React from "react";
import PropTypes from "prop-types";

function Spinner({ color, query, size, ...other }) {
  return (
    <>
      <span
        className="ico-spinner"
        aria-label={`fetching ${query}`}
        style={{
          backgroundColor: color,
          width: `${size}px`,
          height: `${size}px`,
        }}
      />
    </>
  );
}

Spinner.displayName = "IcoSpinner";

Spinner.propTypes = {
  fetching: PropTypes.string,
};

Spinner.defaultProps = {
  fetching: "data",
};

export { Spinner };
