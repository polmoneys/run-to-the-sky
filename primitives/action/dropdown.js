import React from "react";
import {
  Menu,
  MenuList,
  MenuButton,
  MenuItem,
  MenuItems,
  MenuPopover,
  MenuLink,
} from "@reach/menu-button";
import { IcoChevron } from "@/primitives/icon";
import { VisuallyHidden } from "@react-aria/visually-hidden";

const DropDown = ({
  children,
  className = undefined,
  composeClassName = undefined,
  label = "",
  icon = false,
  portal = false,
  direction = "vertical",
}) => {
  const classNames = [
    "drop-down",
    className,
    composeClassName,
    direction === "horizontal" && "is-horizontal",
  ]
    .filter(Boolean)
    .join(" ");
  return (
    <div className={classNames}>
      <Menu>
        {({ isExpanded }) => (
          <>
            <MenuButton>
              {icon ? <VisuallyHidden>{label}</VisuallyHidden> : label}
              {icon ? (
                icon({
                  isExpanded,
                })
              ) : (
                <IcoChevron
                  size={28}
                  viewbox="-2 -4 28 28"
                  transforms={
                    !isExpanded ? undefined : "rotate(180deg) translate(-2px)"
                  }
                />
              )}
            </MenuButton>
            <MenuPopover portal={portal}>
              <MenuItems>{children}</MenuItems>
            </MenuPopover>
          </>
        )}
      </Menu>
    </div>
  );
};

export { DropDown, MenuItem as DropDownItem };
