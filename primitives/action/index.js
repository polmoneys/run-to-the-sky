import React, { useRef } from "react";
import NextLink from "next/link";
import { useButton } from "@react-aria/button";
import {
  LINK_PROPTYPES,
  LINK_DEFAULT_PROPTYPES,
  BUTTON_PROPTYPES,
  BUTTON_DEFAULT_PROPTYPES,
} from "./types";

const Link = (props) => {
  const {
    children,
    className,
    composeClassName,
    target,
    asButton = false,
    ...extra
  } = props;
  const classNames = [!asButton && "is-link", composeClassName, className]
    .filter(Boolean)
    .join(" ");
  const rel = target === "_blank" ? "noopener noreferrer" : undefined;

  return (
    <NextLink {...extra}>
      <a className={classNames} rel={rel}>
        {children}
      </a>
    </NextLink>
  );
};

Link.AsButton = (props) => (
  <Link {...props} asButton composeClassName="action" />
);
Link.propTypes = LINK_PROPTYPES;
Link.defaultProps = LINK_DEFAULT_PROPTYPES;

const Button = (props) => {
  const ref = useRef();
  const { buttonProps } = useButton(props, ref);
  const { className, composeClassName, xs, ...core } = props;
  const classNames = [
    "action main-center cross-center",
    composeClassName,
    className,
    xs && "is-tiny",
  ]
    .filter(Boolean)
    .join(" ");

  const { children, ...aria } = core;

  return (
    <button
      className={classNames}
      ref={ref}
      {...buttonProps}
      {...ariaProps(aria)}
      type="button"
    >
      {children}
    </button>
  );
};
Button.Ghost = (props) => (
  <Button {...props} composeClassName="is-transparent" />
);

Button.Icon = (props) => <Button {...props} composeClassName="col" />;
export { Link, Button };

const ariaProps = (obj) => {
  let newObj = {};
  Object.keys(obj).forEach((prop) => {
    if (prop === "aria-hidden" || prop === "tabIndex" || prop === "id") {
      newObj[prop] = obj[prop];
    }
  });

  return newObj;
};

Button.propTypes = BUTTON_PROPTYPES;
Button.defaultProps = BUTTON_DEFAULT_PROPTYPES;
