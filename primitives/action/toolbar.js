import React from "react";
import { FocusScope } from "@react-aria/focus";
import { ColToRow } from "@/primitives/box";

// TODO: https://react-spectrum.adobe.com/react-aria/FocusScope.html#usefocusmanager-example

const ToolBar = ({
  className = undefined,
  composeClassName = "flex",
  children,
}) => {
  const classNames = ["toolbar", className, composeClassName]
    .filter(Boolean)
    .join(" ");

  return (
    <ColToRow role="toolbar" className={classNames}>
      <FocusScope>{children}</FocusScope>
    </ColToRow>
  );
};

export { ToolBar };
