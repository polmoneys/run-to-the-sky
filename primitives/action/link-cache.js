import React, { useEffect } from "react";
import { HOUR } from "@/tokens";
import { BrowserRouter, useLocation } from "react-router-dom";

const StaleAppRouter = (props) => {
  const [isStale, setIsStale] = useState(false);
  useEffect(() => {
    let id = setTimeout(() => {
      setIsStale(true);
    }, hour * 12);
    return () => clearTimeout(id);
  }, []);
  return <BrowserRouter forceRefresh={isStale} />;
};
// now replace <BrowserRouter/> with <StaleAppRouter/>

// TODO: vs. <Link/> that switches to <a/> once, maybe every 24h
