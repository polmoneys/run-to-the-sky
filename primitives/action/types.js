import PropTypes from "prop-types";

const LINK_PROPTYPES = {
  children: PropTypes.any.isRequired,
  href: PropTypes.string,
  composeClassName: PropTypes.any,
  target: PropTypes.oneOf([undefined, "self", "_blank"]),
};

const LINK_DEFAULT_PROPTYPES = {
  children: undefined,
  href: "./",
  composeClassName: undefined,
  target: undefined,
};

const BUTTON_PROPTYPES = {
  children: PropTypes.any.isRequired,
  composeClassName: PropTypes.any,
  xs: PropTypes.bool,
  disabled: PropTypes.bool,
  onPress: PropTypes.func,
};

const BUTTON_DEFAULT_PROPTYPES = {
  children: undefined,
  disabled: false,
  composeClassName: undefined,
  onPress: () => ({}),
  xs: false,
};

export {
  LINK_PROPTYPES,
  LINK_DEFAULT_PROPTYPES,
  BUTTON_PROPTYPES,
  BUTTON_DEFAULT_PROPTYPES,
};
