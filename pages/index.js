import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import { useState } from "react";
import { PageProviders } from "@/context";
import { CONTENT } from "@/translations";
import { useMessageFormatter } from "@react-aria/i18n";

import { Row, Separator, Spacer } from "@/primitives/box";
import { For } from "@/primitives/for";
import { Pic } from "@/primitives/pic";
import { Seo } from "@/units/seo";
import { Track } from "@/primitives/track";

import { unwrapXS } from "@/utils";
import isMobile from "@/utils/is-mobile";

import useSwr from "swr";
import fetcher from "@/utils/fetcher-swr";

// const Desktop = dynamic(() => import("units/landing/Desktop"));
// const Mobile = dynamic(() => import("units/landing/Mobile"));

const Home = ({ isMobile, isBot, runners, ...extra }) => {
  const { name = "jane" } = extra;

  const [currentSection, setSection] = useState("info");

  const { data, error } = useSwr(
    "/runners/c11ac356-416e-4498-8d9c-bdeb8ccee560",
    fetcher,
  );
  const t = useMessageFormatter(CONTENT);

  function changeSection(entry) {
    setSection(entry);
  }
  return (
    <PageProviders>
      {/* {isMobile ? <Mobile /> : <Desktop />} */}
      <main>
        {data ? (
          <div className="flex  row-gap">
            <p>{data.first_name}</p>
            <Separator.Y />
            <Pic.Avatar src={data.avatar} />
          </div>
        ) : (
          <p>loading</p>
        )}

        <Spacer />
        <Separator />
        <Spacer />

        <For
          className="flex  row-gap"
          of={runners}
          parent="ul"
          iteratee={["first_name", "avatar"]}
        >
          {({ xs }) => (
            <>
              {xs.map((item, i) => (
                <li className="flex col" key={i}>
                  <p>{item.first_name}</p>
                  <Separator.Min />
                  <Pic.AvatarXL src={item.avatar} />
                  <Spacer />
                </li>
              ))}
            </>
          )}
        </For>
        {/* <Track id="info" onIntersect={(id) => changeSection(id)}>
          <h1>{t("greeting", { name })}</h1>
        </Track> */}
      </main>
    </PageProviders>
  );
};

export default Home;

export async function getServerSideProps() {
  try {
    const res = await fetch("https://x.com/runners");
    const runners = await res.json();
    return {
      props: {
        runners,
        isMobile: isMobile(req),
        isBot: isBot(req), // dont render extra data if is a bot
      },
    };
  } catch (error) {
    console.log("=======> o_O SERVER", error.status);
    return {
      props: {
        fail: true,
      },
    };
  }
}
