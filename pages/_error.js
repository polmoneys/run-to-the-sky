import { CONTENT } from "@/translations/_error";

function Error({ statusCode }) {
  const t = useMessageFormatter(CONTENT);

  return <p>{statusCode ? t("server", { statusCode }) : t("client")}</p>;
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export default Error;
