import { useEffect } from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import { AppProviders } from "@/context";
import { Layout as LayoutDefault } from "@/layouts/layout-base";
import { MaintenanceMode } from "@/units/maintenance";

import "@/styles/critical.css"; // FIXME: INLINE

import "@/styles/theme.css";

import "@/primitives/accordion/accordion.css";
import "@/primitives/action/action.css";
import "@/primitives/box/box.css";
import "@/primitives/breadcrumb/breadcrumb.css";
import "@/primitives/form/form.css";
import "@/primitives/form/select/select.css";
import "@/primitives/form/typeahead/typeahead.css";
import "@/primitives/form/input/input.css";
import "@/primitives/form/checkbox/checkbox.css";
import "@/primitives/form/radio/radio.css";
import "@/primitives/form/textarea/textarea.css";
import "@/primitives/pic/pic.css";
import "@/primitives/tabs/tabs.css";
import "@/primitives/table/table.css";
import "@/primitives/text/text.css";
import "@/primitives/overlay/overlays.css";
import "@/units/cookie/cookie.css";
import "@/units/nav/nav.css";
import "@/units/card/card.css";

if (process.env.NEXT_PUBLIC_API_MOCKING === "enabled") {
  require("../mocks");
}

/*
TODO: READ @/context/Readme.md

*/

function MyApp({ Component, pageProps }) {
  const { locale } = useRouter();
  // const { direction } = useLocale();

  // useEffect(() => {
  //   document.querySelector("html")?.setAttribute("dir", direction);
  // }, [locale]);

  const Layout = Component.Layout || LayoutDefault;

  // if (process.env.NEXT_PUBLIC_MAINTENANCE_MODE) return <MaintenanceMode />;

  return (
    <AppProviders initialLocale={locale}>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1"
          key="key-viewport"
        />
        <title>UTMB global</title>
      </Head>
      <Layout>
        <Component {...pageProps} locale={locale} />
      </Layout>
    </AppProviders>
  );
}

export default MyApp;
