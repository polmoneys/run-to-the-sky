import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import { useState } from "react";
import { Seo } from "@/units/seo";
import { Track } from "@/primitives/track";
import { PageProviders } from "@/context";
import { Layout } from "@/layouts/layout-dashboard";
import { useLocale } from "@react-aria/i18n";
import { useMessageFormatter } from "@react-aria/i18n";
import { CONTENT } from "@/translations/discover";

const Discover = (props) => {
  const { name = "jane" } = props;

  const { locale, direction } = useLocale();

  const [currentSection, setSection] = useState("info");
  const t = useMessageFormatter(CONTENT);

  function changeSection(entry) {
    setSection(entry);
  }

  return (
    <PageProviders initialLocale={locale}>
      <main>
        <p>It works</p>
        {/* <Track id="info" onIntersect={(id) => changeSection(id)}>
          <h1>{t("greeting", { name })}</h1>
        </Track> */}
      </main>
    </PageProviders>
  );
};

Discover.Layout = Layout;

export default Discover;
