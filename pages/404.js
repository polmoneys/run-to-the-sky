import { Link, Button } from "@/primitives/action";
import { ColToRow, Row, Spacer } from "@/primitives/box";

export default function Custom404() {
  return (
    <>
      <Spacer size="xl" />
      <Spacer size="xl" />
      <Row tag="nav" className="landscape -light main-end cross-center ">
        <Link href="/">Back</Link>
      </Row>
      <main>
        <ColToRow className="main-between cross-center">
          <h1 className="h1 font-bold">404</h1>
          <p className="font-bold -violet"> #allgoodfriend</p>
        </ColToRow>
        <Spacer />
        <p>
          Veuillez nous excuser, la page demandée n'existe pas ou plus, ou l'URL
          que vous avez saisie est erronée.
        </p>
        <Spacer size="xl" />
      </main>
    </>
  );
}
