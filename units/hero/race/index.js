import { Pic } from "@/primitives/pic";
import { Pin } from "@/primitives/box";
import { IcoArrow } from "@/primitives/ico";
import { Text } from "@/primitives/text";
import { SocialIcons } from "@/units/social";
import { Slides } from "@/primitives/slides";
import { CardFlash } from "@/units/cards";
import { Hero } from "@/units/hero";

Pic.Stretch = (props) => <Pic composeClassName="is-absolute-fill has-blend" />;

const LandingRace = (props) => {
  return (
    <Hero.Race>
      <p className="futur-heavy-16 font-uppercase"> AUGUST 29, 2021</p>
      <p className="hero-title">OCC</p>
      <Text.Lorem className="futur-light-36" />

      <Button className="is-bg-white futura-heavy-16">
        Register now
        <IcoArrow transform="right" />
      </Button>

      <div className="flex main-start">
        <Stat className="self">
          <p className="spacemono-book-18 letter-spacing-0-78px font-uppercas">
            Running stones
          </p>
          <p className="spacemono-bold-24 is-accent-#e20613">30</p>
        </Stat>

        <Stat className="self">
          <p className="spacemono-book-18 letter-spacing-0-78px font-uppercas">
            Intra points
          </p>
          <p className="spacemono-bold-24 is-accent-#e20613">10</p>
        </Stat>
      </div>
      <Pic.Stretch />
    </Hero.Race>
  );
};

const StoryRace = (props) => {
  return (
    <Hero.Story>
      <Pin className="is-absolute-center">
        <NumberSvg />
      </Pin>
      <Slides>
        {MOCK.map((r) => (
          <div className="hero-race-story-content">
            <Text.Quote />
            <Pic.Stretch />
            <Pin className="here-or-pic-width-height-size">
              <Pic />
            </Pin>
          </div>
        ))}
        <SlidesBlobControls />
      </Slides>
    </Hero.Story>
  );
};
