import { Pic } from "@/primitives/pic";
import { Pin } from "@/primitives/box";
import { IcoScrollDown } from "@/primitives/ico";
import { Text } from "@/primitives/text";
import { SocialIcons } from "@/units/social";
import { Slides } from "@/primitives/slides";
import { CardFlash } from "@/units/cards";
import { Hero } from "@/units/hero";

const UID = 0;
const LOREM =
  "Lorem ipsum dolor sit amet indiscliplinctur gloria at est, Conrad.";
const MOCK = [
  {
    id: ++UID,
    title: "UTMB &#xA9;",
    description: LOREM,
  },
  {
    id: ++UID,
    title: "UTMB &#xA9;",
    description: LOREM,
  },
];

Pic.Stretch = (props) => <Pic composeClassName="is-absolute-fill has-blend" />;

/*
has blend overlay
  opacity: 0.55;
  background-blend-mode: multiply;
  background-image: linear-gradient(to bottom, #6f6f6f, #6f6f6f);
*/

const Landing = (props) => {
  return (
    <Hero.Landing>
      <p className="futur-heavy-16">UTMB&#xA9; MONT-BLANC</p>
      <p className="hero-title">THE MYTH</p>
      <Text.Lorem className="futur-light-36" />
      <p className="futur-heavy-16">Be part</p>
      <div className="separator is-small" aria-hidden="true" />
      <IcoScrollDown />
      <SocialIcons orientation="vertical" />
      <Pic.Stretch />
      <Pin pos="bottom-right">
        <Slides>
          {MOCK.map((n) => (
            <CardFlash {...n} />
          ))}
        </Slides>
      </Pin>
    </Hero.Landing>
  );
};
