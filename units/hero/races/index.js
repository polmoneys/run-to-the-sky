import { Pic } from "@/primitives/pic";
import { Pin } from "@/primitives/box";
import { IcoArrow } from "@/primitives/ico";
import { Text } from "@/primitives/text";
import { SocialIcons } from "@/units/social";
import { Slides } from "@/primitives/slides";
import { CardFlash } from "@/units/cards";
import { Hero } from "@/units/hero";

// TODO: MOBILE HIDE STUFF ?¿
const UID = 0;
const LOREM =
  "Lorem ipsum dolor sit amet indiscliplinctur gloria at est, Conrad.";
const MOCK = [
  {
    id: ++UID,
    title: "UTMB",
    summary: LOREM,
    to: "/slug",
    stones: 30,
    points: 10,
    dist: "170km",
    elevation: "10.000M",
    finishers: "85%",
    description: LOREM,
  },
];

Pic.Stretch = (props) => <Pic composeClassName="is-absolute-fill has-blend" />;

const Races = (props) => {
  return (
    <Hero.Races>
      <p className="futur-medium-32">RACES</p>
      <Text.Lorem className="futur-light-36" />

      <div className="flex main-start">
        <p className="futura-heavy-16 is-accent">UTMB&#xA9;</p>
        <p className="futura-heavy-16">OCC</p>
        <p className="futura-heavy-16">TDS</p>
        <p className="futura-heavy-16">MCC</p>
        <p className="futura-heavy-16">YCC</p>
        <p className="futura-heavy-16">PTL</p>
        <Link className="self-margin-left futura-book-16">
          Compare all races
          <IcoArrow transform="right" />
        </Link>
      </div>
      <div className="slide-progress" aria-hidden="true" />
      <Slides>
        {MOCK.map((r) => (
          <div className="hero-race-content">
            <p className="futur-book-32">RACES</p>
            <Text.Lorem className="futur-book-18" />
            <Link className="self-margin-left futura-book-16 has-underline">
              Race details
              <IcoArrow transform="right" />
            </Link>

            <div className="row main-start">
              <Stat className="self">
                <p className="spacemono-book-18 letter-spacing-0-78px font-uppercas">
                  Running stones
                </p>
                <p className="spacemono-bold-24 is-accent-#e20613">
                  {r.stones}
                </p>
              </Stat>

              <Stat className="self">
                <p className="spacemono-book-18 letter-spacing-0-78px font-uppercas">
                  Itra points
                </p>
                <p className="spacemono-bold-24 is-accent-#e20613">
                  {r.points}
                </p>
              </Stat>
              <div className="self is-xl is-bg-accent is-white has-padding-92-75-42-70">
                <p className="spacemono-bold-18 letter-spacing-0-78px font-uppercas">
                  The Race
                </p>

                <RaceSvg />

                <div className="row main-start">
                  <Stat className="self">
                    <p className="spacemono-book-18 letter-spacing-0-78px font-uppercas">
                      Dist
                    </p>
                    <p className="spacemono-bold-24 is-accent-#e20613">
                      {r.dist}
                    </p>
                  </Stat>
                  <Stat className="self">
                    <p className="spacemono-book-18 letter-spacing-0-78px font-uppercas">
                      Elevation gain
                    </p>
                    <p className="spacemono-bold-24 is-accent-#e20613">
                      {r.elevation}
                    </p>
                  </Stat>
                  <Stat className="self">
                    <p className="spacemono-book-18 letter-spacing-0-78px font-uppercas">
                      Finishers
                    </p>
                    <p className="spacemono-bold-24 is-accent-#e20613">
                      {r.finishers}
                    </p>
                  </Stat>
                </div>
              </div>
            </div>

            <Pin className="here-or-pic-width-height-size">
              <Pic />
              <Button className="is-bg-accent futura-heavy-16">
                Race details
                <IcoArrow transform="right" />
              </Button>
            </Pin>
          </div>
        ))}
      </Slides>
    </Hero.Races>
  );
};
