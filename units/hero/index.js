import { Box } from "@/primitives/box";

const Hero = (props) => <Box composeClassName="hero is-relative" {...props} />;
Hero.Landing = (props) => (
  <Box as="aside" composeClassName="hero is-relative hero-landing" {...props} />
);
Hero.Intro = (props) => (
  <Box as="aside" composeClassName="hero is-relative hero-intro" {...props} />
);
Hero.Race = (props) => (
  <Box as="aside" composeClassName="hero is-relative hero-race" {...props} />
);
Hero.Story = (props) => (
  <Box as="aside" composeClassName="hero is-relative" {...props} />
);
Hero.Experiences = (props) => (
  <Box
    as="aside"
    composeClassName="hero is-relative hero-experiences"
    {...props}
  />
);
Hero.Races = (props) => (
  <Box
    as="aside"
    composeClassName="hero is-relative hero-races with-bg-image-or-inline-aria-hidden"
    {...props}
  />
);

// TODO: BOOM SSR FETCH and then <Perf/> a la <Seo/> to
// enchance Head at HEROS <link rel="preload" as="image" href="poster.jpg"/>

export { Hero };
