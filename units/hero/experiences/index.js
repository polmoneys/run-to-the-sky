import { Pic } from "@/primitives/pic";
import { Pin } from "@/primitives/box";
import { IcoArrow } from "@/primitives/ico";
import { Text } from "@/primitives/text";
import { SocialIcons } from "@/units/social";
import { Slides } from "@/primitives/slides";
import { CardFlash } from "@/units/cards";
import { Hero } from "@/units/hero";

// TODO: MOBILE HIDE STUFF ?¿
const UID = 0;
const LOREM =
  "Lorem ipsum dolor sit amet indiscliplinctur gloria at est, Conrad.";
const MOCK = [
  {
    id: ++UID,
    title: "UTMB",
    summary: LOREM,
    to: "/slug",
    stones: 30,
    points: 10,
    dist: "170km",
    elevation: "10.000M",
    finishers: "85%",
    description: LOREM,
  },
];

Pic.Stretch = (props) => <Pic composeClassName="is-absolute-fill has-blend" />;

/*
has overlay
  opacity: 0.5;
  background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), #000000 98%);
*/
const You = (props) => {
  return (
    <Hero.Experiences>
      <p className="futur-medium-32">CHOOSE YOUR QUEST</p>

      <div className="flex main-center">
        <p className="futura-book-16 is-active">RUNNER</p>
        <p className="futura-book-16 opacity-6">VOLUNTEER</p>
        <p className="futura-book-16 opacity-6">SUPORTER</p>
        <p className="futura-book-16 opacity-6">PARTNER</p>
        <p className="futura-book-16 opacity-6">PRESS</p>
      </div>
      <div className="slide-progress" aria-hidden="true" />

      <Pin className="is-absolute-center">
        <NumberSvg />
      </Pin>

      <Slides>
        {MOCK.map((r) => (
          <div className="hero-race-content">
            <Text.Lorem className="futur-book-20" />
            <Button className="is-bg-white futura-heavy-16">
              Register now
              <IcoArrow transform="right" />
            </Button>
            <Pic.Stretch />
            <Pin className="here-or-pic-width-height-size">
              <Pic />
            </Pin>
          </div>
        ))}
        <SlidesBlobControls />
      </Slides>
    </Hero.Experiences>
  );
};
