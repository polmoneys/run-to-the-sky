import { Pic } from "@/primitives/pic";
import { Pin } from "@/primitives/box";
import { IcoScrollDown } from "@/primitives/ico";
import { Text } from "@/primitives/text";
import { SocialIcons } from "@/units/social";
import { Hero } from "@/units/hero";

/*
has overlay
opacity: 0.4;
background - blend - mode: multiply;
background - image: linear - gradient(to bottom, rgba(255, 255, 255, 0), #000000);
*/
const Access = (props) => {
  return (
    <Hero>
      <p className="futur-medium-32">ACCESS</p>
      <Text.Lorem className="futur-book-18" />

      <Pic />
    </Hero>
  );
};

/*
has overlay
opacity: 0.3;
  background-blend-mode: multiply;
  background-image: linear-gradient(183deg, rgba(255, 255, 255, 0) 242%, rgba(0, 0, 0, 0.96) 80%);
*/

const Register = (props) => {
  return (
    <Hero>
      <p className="futur-medium-32">ACCESS</p>
      <Text.Lorem className="futur-book-18" />
      <Button className="is-bg-white futura-heavy-16">
        Register now
        <IcoArrow transform="right" />
      </Button>
      <Pic />
    </Hero>
  );
};

const Races = (props) => {
  return (
    <Hero>
      <p className="futur-medium-32">ACCESS</p>
      <Text.Lorem className="futur-book-18" />

      <Pic />
      <Pin className="here-or-pic-width-height-size">
        <Pic />
      </Pin>
    </Hero>
  );
};

const PostX = (props) => {
  return (
    <Hero>
      <Pic />
      <Pin className="here-or-pic-width-height-size">
        <Pic />
      </Pin>
      <SocialIcons orientation="vertical" />
    </Hero>
  );
};
