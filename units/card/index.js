/**
 * 

HERO MOUNTAIN https://images.unsplash.com/photo-1443890923422-7819ed4101c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2560&q=80

MOUNTAIN WITH RUNNER https://images.unsplash.com/photo-1530143311094-34d807799e8f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjI2Mjc0fQ&auto=format&fit=crop&w=2550&q=80
 * 
 * 
    <div className="has-blend"">
        <Pic url="https://images.unsplash.com/photo-1593461977713-149117742843?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" />
        <Text className="card-title-transparent">title</Text>
    <<div className="has-blend"">
 */

/*
has blend/overlay => on hover remove IT
background-image: linear-gradient(to bottom, rgba(255, 255, 255, 0) -24%, #41505c 91%);
*/

/*
has blend/overlay 
  background-blend-mode: multiply;
  background-image: linear-gradient(to bottom, rgba(255, 255, 255, 0) -24%, #000000 91%);

on hover
  background-blend-mode: multiply;
  background-image: linear-gradient(to bottom, rgba(255, 255, 255, 0) -24%, #41505c 91%);
*/

/*
<Card.Strip className="flex  main-between">
  <Pic className="max-height" />
    <div>
      <SpaceRegular14 className="font-uppercase">OFFER</SpaceRegular14>
      <FuturaHeavy16 className="font-uppercase">LOREM IPSUN DOLOR</FuturaHeavy16>

      <FuturaMedium20><Text.LoremXL/> </FuturaMedium20>
      <div className="flex main-end">
        <IcoArrow transform="right" />
      </div>
    </div>
</Card.Strip>
*/
