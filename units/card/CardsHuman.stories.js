import React, { useEffect } from "react";
import { Meta, Story, Source, Canvas } from "@storybook/addon-docs/blocks";
import { useHover } from "@react-aria/interactions";
import { Pin, Br, Grid, Spacer, Separator, Row, Col } from "@/primitives/box";
import { Text } from "@/primitives/text";
import { Font } from "@/primitives/text/font";
import { IcoMoreV, IcoCircle } from "@/primitives/icon";
import { Flag } from "@/primitives/icon/flag";
import { Button, Link } from "@/primitives/action";
import { DropDown, DropDownItem } from "@/primitives/action/dropdown";
import { VisuallyHidden } from "@react-aria/visually-hidden";

//TODO: INTEGRATE WITH NEXT/IMAGE

export const OneCardHumans = ({ of = [1, 2, 3, 4] }) => (
  <>
    <h1>A card it's just a box with a ratio</h1>
    <Spacer />
    <CardHumanTall />
    <Spacer />
    <CardHumanProfile />
    <Spacer />
    <CardHumanNotTall />
  </>
);

export const OneListHumans = ({ of = [1, 2, 3, 4, 5, 6, 7, 8] }) => (
  <>
    <h1>A list of cards</h1>
    <Spacer />
    <Grid className="grid-gap">
      {of.map((i) => (
        <CardHumanTall key={i} />
      ))}
    </Grid>
    <Spacer />
    <Spacer />
    <Spacer />

    <Grid className="grid-gap">
      {of
        .filter((x, i) => i <= 3)
        .map((i) => (
          <CardHumanNotTall key={i} />
        ))}
    </Grid>
  </>
);

const CardHumanTall = (props) => {
  const { className = undefined, ...datum } = props;
  const { hoverProps, isHovered } = useHover({
    onHoverStart: (e) => ({}),
    onHoverEnd: (e) => ({}),
  });

  const classNames = [
    className,
    "main-end ratio is-human-tall has-p-0-0-35-38  is-white",
    isHovered && "has-hover",
  ]
    .filter(Boolean)
    .join(" ");

  const {
    nationality = "https://i.imgur.com/zY5yClC.png",
    first_name = "Kristin ",
    last_name = "Berglund",
    itra = 900,
    src = "https://images.unsplash.com/photo-1522040942177-269680274214?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80",
  } = datum;

  return (
    <Col {...hoverProps} className={classNames}>
      <img
        className="ratio-item"
        alt={`${first_name} ${last_name} portrait`}
        src={src}
        aria-hidden="true"
      />
      <Font.FuturaBook16 className="font-uppercase">
        {first_name}
        <Br />
        {last_name}
      </Font.FuturaBook16>
      {isHovered && (
        <>
          <Font.SpaceRegular14>ITRA : {itra}</Font.SpaceRegular14>
          <Flag flag={nationality} />
          <Font.FuturaBook16>
            <Link className="font-underline">Voir le profil</Link>
          </Font.FuturaBook16>
        </>
      )}
    </Col>
  );
};

const CardHumanNotTall = (props) => {
  const { className = undefined, ...datum } = props;
  const { hoverProps, isHovered } = useHover({
    onHoverStart: (e) => ({}),
    onHoverEnd: (e) => ({}),
  });

  const classNames = [
    className,
    "main-end ratio is-human-not-tall has-p-0-0-35-38  is-white",
    isHovered && "has-hover",
  ]
    .filter(Boolean)
    .join(" ");

  const {
    label = "Supporter",
    to = "./supporter",
    src = "https://images.unsplash.com/photo-1522040942177-269680274214?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80",
  } = datum;

  return (
    <Col {...hoverProps} className={classNames}>
      <img className="ratio-item" alt={label} src={src} aria-hidden="true" />

      <Font.FuturaBook16>
        <Link className="font-underline" href={to}>
          {label}
        </Link>
      </Font.FuturaBook16>
    </Col>
  );
};

const CardHumanProfile = (props) => {
  const { className = undefined, ...datum } = props;
  const classNames = [
    className,
    "main-end cross-end ratio is-human-profile white ",
  ]
    .filter(Boolean)
    .join(" ");

  const {
    label = "Supporter",
    social = {
      twitter: "https://twitter.com",
      instagram: "https://twitter.com",
      other: "https://twitter.com",
    },
    src = "https://images.unsplash.com/photo-1602174940488-1fe0e9208fb4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  } = datum;

  const openExternalLink = (to) => console.log(to); // FIXME: PUSH ROUTER NEW WINDOW

  return (
    <Col className={classNames}>
      <img className="ratio-item" src={src} />
      <DropDown
        className="flex main-end"
        direction="horizontal"
        icon={({ isExpanded }) => (
          <IcoMoreV
          size={28}
          viewbox="-1.4 -1.3 18 18"
          color={isExpanded ? "coral" : "black"}
        />
      )}
    >
      {Object.entries(social).map(([key, to]) => (
        <DropDownItem
          className="flex main-between cross-center"
          onSelect={() => openExternalLink(to)}
        >
          <IcoCircle size={32} viewbox="-1.4 -1.3 18 18" />
          <VisuallyHidden>{key}</VisuallyHidden>
        </DropDownItem>
      ))}
    </DropDown>
    </Col>
  );
};
