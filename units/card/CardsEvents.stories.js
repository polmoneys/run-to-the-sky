import React, { useEffect } from "react";
import { Meta, Story, Source, Canvas } from "@storybook/addon-docs/blocks";
import { useHover } from "@react-aria/interactions";
import { Pin, Br, Grid, Spacer, Separator, Row, Col } from "@/primitives/box";
import { Text } from "@/primitives/text";
import { Font } from "@/primitives/text/font";
import { IcoMoreV, IcoCircle } from "@/primitives/icon";
import { Flag } from "@/primitives/icon/flag";
import { Button, Link } from "@/primitives/action";
import { DropDown, DropDownItem } from "@/primitives/action/dropdown";
import { VisuallyHidden } from "@react-aria/visually-hidden";
import { ToolBar } from "@/primitives/action/toolbar";

//TODO: INTEGRATE WITH NEXT/IMAGE

export const OneCardEvents = ({ of = [1, 2, 3, 4] }) => (
  <>
    <h1>A card it's just a box with a ratio</h1>
    <Spacer />
    <CardEvent />
    <Spacer />
    <Spacer />
    <CardEventGallery />
    <Spacer />
  </>
);

const CardEvent = (props) => {
  const {
    title = "MONT-BLANC TOUR",
    summary = "Lorem ipsun dolor sit amet indisciplinctur morning sunshine. Lorem ipsun dolor sit amet indisciplinctur morning sunshine. Lorem ipsun dolor sit sunshine. Lorem ipsun dolor sit amet indisciplinctur morning sunshine.",
    post_date = "NOV 24TH",
    src = "https://images.unsplash.com/photo-1590333748338-d629e4564ad9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2618&q=80",
  } = props;

  const maxLength = 99;

  return (
    <Col className="is-event">
      <img alt={title} src={src} aria-hidden="true" />
      <div className="has-p-0-25-0-25">
        <Spacer />
        <Row className="main-between cross-start">
          <Font.SpaceBold16>{post_date}</Font.SpaceBold16>
          <ToolBar className="is-bg-transparent">
            <Button.Ghost onPress={() => console.log("TO DO")}>
              <IcoCircle size={32} viewbox="-1.4 -1.3 18 18" />
            </Button.Ghost>
            <Button.Ghost onPress={() => console.log("TO DO")}>
              <IcoCircle size={32} viewbox="-1.4 -1.3 18 18" />
            </Button.Ghost>
          </ToolBar>
        </Row>
        <Spacer />
        <Font.FuturaBook18>{title}</Font.FuturaBook18>
        <Font.FuturaMedium16>
          {summary.length > maxLength
            ? `${summary.substr(0, maxLength).trim()}...`
            : summary}
        </Font.FuturaMedium16>
        <Spacer />
      </div>
    </Col>
  );
};

const CardEventGallery = (props) => {
  const {
    title = "MONT-BLANC TOUR",
    summary = "Lorem ipsun dolor sit amet indisciplinctur morning sunshine. Lorem ipsun dolor sit amet indisciplinctur morning sunshine. Lorem ipsun dolor sit sunshine. Lorem ipsun dolor sit amet indisciplinctur morning sunshine.",
    src = "https://images.unsplash.com/photo-1602174940488-1fe0e9208fb4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  } = props;

  const maxLength = 99;

  return (
    <Col className="is-event-gallery">
      <img src={src} />
      <Pin className="is-bg-grey" xy={{ top: "220px", right: 0 }}>
        <IcoCircle size={36} viewbox="-1.4 -1.3 18 18" color="white" />
      </Pin>
      <Spacer />
      <Font.FuturaBook18>{title}</Font.FuturaBook18>
      <Font.FuturaMedium16>
        {summary.length > maxLength
          ? `${summary.substr(0, maxLength).trim()}...`
          : summary}
      </Font.FuturaMedium16>
      <Spacer />
    </Col>
  );
};

//FIXME:  It's an image
// has dropdown vertical top right (share,download/use as profile pic)
// has badge bottom right

const CardEventMedia = (props) => {
  const {
    actions= {
      share:()=>console.log('share'),
      download:()=>console.log('download'),
      profile:()=>console.log('use as profile'),
    },
    alt = "Image alternate text",
    src = "https://images.unsplash.com/photo-1602174940488-1fe0e9208fb4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  } = props;

  return (
    <Col>
      <img src={src} className="ratio-image" />
      <Pin className="is-bg-grey" xy={{ top: "calc(100% - 44px)", right: 0 }}>
        <IcoCircle size={36} viewbox="-1.4 -1.3 18 18" color="white" />
      </Pin>   
      <Pin xy={{ top: 0, right: 0 }}>

      <DropDown
        className="is-tiny"
        icon={({ isExpanded }) => (
          <IcoMoreV
          size={28}
          viewbox="-1.4 -1.3 18 18"
          color={isExpanded ? "coral" : "black"}
        />
      )}
    >
      {Object.entries(actions).map(([key, cb]) => (
        <DropDownItem
          className="flex main-center cross-center"
          onSelect={() => cb(key)}
        >
          <IcoCircle size={32} viewbox="-1.4 -1.3 18 18" />
          <VisuallyHidden>{key}</VisuallyHidden>
        </DropDownItem>
      ))}
    </DropDown>
    </Pin>   
    </Col>
  );
};
