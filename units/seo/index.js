import { CONTENT } from "./translation";
import { useLocale } from "@react-aria/i18n";
import { useMessageFormatter } from "@react-aria/i18n";

/*

https://github.com/garmeeh/next-seo

TIP dev tools:
console.table(
    Array
        .from(document.querySelectorAll('meta'))
        .map(meta => ({
            name: meta.getAttribute('property') || meta.getAttribute('name'),
            content: meta.getAttribute('content')
        }))
)

*/

const Seo = (props) => {
  const { locale, direction } = useLocale();

  const t = useMessageFormatter(CONTENT);

  return (
    <>
      <meta charSet="utf-8" key="key-charset" />
      <meta
        name="apple-mobile-web-app-capable"
        content="no"
        key="key-app-capable"
      />
      <meta name="googlebot" content="index,follow" key="key-google-bot" />
      <meta name="robots" content="index,follow" key="key-bots" />
      <meta name="rating" content="general" key="key-rating" />

      <meta name="title" content={t("title")} key="key-meta-title" />
      <link rel="shortcut icon" href="/favicon.ico" key="key-favicon" />
      <link rel="canonical" href={t("url")} key="key-canonical" />
      <meta property="og:locale" content={locale} key="key-locale" />
      <meta
        name="description"
        content={t("description")}
        key="key-description"
      />
      <meta name="keywords" content={t("keywords")} key="key-keywords" />
      <meta name="contact" content={t("contact")} key="key-contact" />
      <meta name="copyright" content={t("copyright")} key="key-copyright" />
      <meta name="og:url" content={t("url")} key="key-og-url" />
      <meta name="og:title" content={t("title")} key="key-og-title" />
      <meta
        name="og:description"
        content={t("description")}
        key="key-og-description"
      />
      <meta name="og:type" content="website" key="key-og-type" />
      <meta name="twitter:title" content={t("title")} key="key-twitter-title" />
      <meta
        name="twitter:description"
        content={t("description")}
        key="key-twitter-description"
      />
      <meta
        name="twitter:card"
        content="summary_large_image"
        key="key-twitter-image-large"
      />
      <meta name="twitter:url" content={t("url")} key="key-twitter-url" />
      {/*
      <meta name="twitter:image" content="/landscape.jpg" key="key-twitter-image" />
      <meta name="og:image" content="/landscape.jpg"key="key-og-image"  /> 
      */}
    </>
  );
};

export { Seo };
