export const defaultDirection = "ltr";
const copyRightToday = new Date();
const copyRightYear = copyRightToday.getFullYear();

export const CONTENT = Object.freeze({
  "en-EN": {
    title: "Utmb World",
    description: "en-locale description",
    keywords: "hell world",
    url: "https://utmbworld.com/",
    contact: "support@utmbworld.com",
    copyright: `© COPYRIGHT ${copyRightYear}`,
  },
  "en-US": {
    title: "Utmb World",
    description: "en-locale description",
    keywords: "hell world",
    url: "https://utmbworld.com/",
    contact: "support@utmbworld.com",
    copyright: `© COPYRIGHT ${copyRightYear}`,
  },
  "fr-FR": {
    title: "Utmb World",
    description: "fr-locale description",
    keywords: "oh la la",
    url: "https://utmbworld.com/",
    contact: "support@utmbworld.com",
    copyright: `© TOUT DROIS RESERVEES ${copyRightYear}`,
  },
  "es-ES": {
    title: "Utmb World",
    description: "es-locale descrición",
    keywords: "ole ole",
    url: "https://utmbworld.com/",
    contact: "support@utmbworld.com",
    copyright: `© TODOS LOS DERECHOS RESERVADOS ${copyRightYear}`,
  },
});
