const Slide = ({ title, url, className }) => {
  return (
    <div className={className}>
      <p>{title}</p>
      <img src={url} alt={title} />
    </div>
  );
};

export { Slide };
