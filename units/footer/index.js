import { useMessageFormatter } from "@react-aria/i18n";
import { CONTENT } from "./translation";
import { Pic } from "@/primitives/pic";
import { Link, Button } from "@/primitives/action";
import { Row, Col, Separator } from "@/primitives/box";

const copyRightToday = new Date();
const copyRightYear = copyRightToday.getFullYear();

const Footer = () => {
  const t = useMessageFormatter(CONTENT);
  return (
    <Col as="footer">
      <Row>
        <Link href="/">
          <Pic.Logo />
        </Link>
        <Col>
          <Separator size="xxs" />
          <p>{t("helpTitle")}</p>
          <p>{t("sponsorship")}</p>
          <p>{t("volunteer")}</p>
          <p>{t("press")}</p>
        </Col>
        <Col>
          <Separator size="xxs" />
          <p>{t("contactTitle")}</p>
          <p>{t("contact")}</p>
          <p>{t("newsletter")}</p>
        </Col>
        <Col>
          <Separator size="xxs" />
          <p>{t("followTitle")}</p>
          {/* SOCIAL MEDIA HORIZONTAL */}
        </Col>
      </Row>
      {/* <Separator size="full"/> */}
      <Row>
        {/* empty space === <Pic.Logo> */}
        <p>{t("terms")}</p>
        <p>{t("privacy")}</p>
        <p>{t("copyright", { copyRightYear })}</p>
      </Row>
    </Col>
  );
};

export { Footer };
