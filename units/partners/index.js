import { For } from "@/primitives/for";
import { Pic } from "@/primitives/pic";
import { paginate } from "@/utils/index";

const Partners = (props) => {
  const chunks = paginate(props.of, 5); // TODO: mobile is same ?
  return (
    <div className="partners-grid">
      {chunks.map((chunk) => (
        <For of={chunk} parent="ul" iteratee={["url", "alt"]}>
          {({ xs }) => (
            <>
              {xs.map((item, i) => (
                <li className="flex" key={i}>
                  <Pic src={item.url} alt={item.alt} />
                </li>
              ))}
            </>
          )}
        </For>
      ))}
    </div>
  );
};

export { Partners };
