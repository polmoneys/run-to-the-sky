export const CONTENT = Object.freeze({
  en: {
    message:
      "Your browser is not supported. Please choose an alternative or use you mobile phone.",
  },
  fr: {
    message:
      "Your browser is not supported. Please choose an alternative or use you mobile phone.",
  },
});
