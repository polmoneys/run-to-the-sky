import { CONTENT } from "./translation";
import { useMessageFormatter } from "@react-aria/i18n";

const UnsupportedBrowserMessage = ({ locale }) => {
  const t = useMessageFormatter(CONTENT);

  return (
    <div className="unsupported-experience" aria-hidden="true">
      <p>{t("message")}</p>
    </div>
  );
};

export { UnsupportedBrowserMessage };
