export const CONTENT = Object.freeze({
  "en-US": {
    a: "My Dashboard",
    b: "My Agenda",
    c: "My Achievements",
    d: "My Catalog",
    e: "My Gallery",
    f: "My Documents",
    g: "My Selection",
    h: "My Informations",
    i: "My Contacts",
  },
  "en-EN": {
    a: "My Dashboard",
    b: "My Agenda",
    c: "My Achievements",
    d: "My Catalog",
    e: "My Gallery",
    f: "My Documents",
    g: "My Selection",
    h: "My Informations",
    i: "My Contacts",
  },
  "fr-FR": {
    a: "My Dashboard",
    b: "My Agenda",
    c: "My Achievements",
    d: "My Catalog",
    e: "My Gallery",
    f: "My Documents",
    g: "My Selection",
    h: "My Informations",
    i: "My Contacts",
  },
  "es-ES": {
    a: "My Dashboard",
    b: "My Agenda",
    c: "My Achievements",
    d: "My Catalog",
    e: "My Gallery",
    f: "My Documents",
    g: "My Selection",
    h: "My Informations",
    i: "My Contacts",
  },
});
