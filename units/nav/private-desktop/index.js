import { Pic } from "@/primitives/pic";
import { Link, Button } from "@/primitives/action";
import { Separator } from "@/primitives/separator";
import { Row, Column } from "@/primitives/box";
import { CONTENT } from "./translation";
import { useMessageFormatter } from "@react-aria/i18n";
import { useRouter } from "next/router";

// TODO:STYLES .nav-desktop-private

// TODO: IT EXPANDS ON HOVER position absolute

// TODO: ACTIVELINK
const ActiveLink = ({ children, href, className }) => {
  const router = useRouter();
  return (
    <Link href={href} scroll={false}>
      <a
        className={`${
          router.pathname === href
            ? "text-gray-900 border-gray-800"
            : "text-gray-600 hover:text-gray-700 border-transparent"
        } ${className} block pb-4 font-semibold text-sm sm:text-base border-b-2 focus:outline-none focus:text-gray-900 whitespace-no-wrap`}
      >
        {children}
      </a>
    </Link>
  );
};

const Menu = (props) => {
  const { children = null } = props;
  return (
    <>
      <Column as="nav" className="nav-desktop-private">
        <Link href="/">
          <Pic.LogoDashboard />
        </Link>
        <LinksColumn {...props} />
      </Column>
    </>
  );
};

// TODO: IMPORT ICONS
const FakeIco = () => <span>x</span>;

const LinksColumn = (props) => {
  const t = useMessageFormatter(CONTENT);

  // TODO: CURRENT .is-active border-right/:after
  // TODO: https://flaviocopes.com/nextjs-active-link/

  return (
    <Row className="self-stretch">
      <Link href="/dashboard">
        <FakeIco />
        {t("a")}
      </Link>
      <Link href="/agenda">
        <FakeIco />
        {t("b")}
      </Link>
      <Link href="/achievements">
        <FakeIco />
        {t("c")}
      </Link>
      <Link href="/catalog">
        <FakeIco />
        {t("d")}
      </Link>
      <Link href="/gallery">
        <FakeIco />
        {t("e")}
      </Link>
      <Link href="/documents">
        <FakeIco />
        {t("f")}
      </Link>
      <Link href="/selection">
        <FakeIco />
        {t("g")}
      </Link>
      <Link href="/informations">
        <FakeIco />
        {t("h")}
      </Link>
      <Link href="/contacts">
        <FakeIco />
        {t("i")}
      </Link>
    </Row>
  );
};

const ProgressBar = () => {
  // TODO: useRouter hook + some <Paint/> or useMeter
  return null;
};

const LandingMenu = (props) => {
  return (
    <Row as="nav" className="nav-desktop">
      <Link href="/">
        <Pic />
      </Link>
      <Column>
        <LinksRow {...props} />
        <Separator />
      </Column>
    </Row>
  );
};

Menu.Landing = (props) => <LandingMenu {...props} />;

export { Menu as DesktopNav };
