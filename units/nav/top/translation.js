export const CONTENT = Object.freeze({
  "en-US": {
    logo: "UTMB WORLD",
    cta: "My quest",
  },
  "fr-FR": {
    logo: "UTMB WORLD",
    cta: "My quest",
  },
  "es-ES": {
    logo: "UTMB WORLD",
    cta: "My quest",
  },
});
