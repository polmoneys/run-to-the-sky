import { Pic } from "@/primitives/pic";
import { Link, Button } from "@/primitives/action";
import { Separator } from "@/primitives/separator";
import { Row, Column } from "@/primitives/box";
import { CONTENT } from "./translation";
import { useMessageFormatter } from "@react-aria/i18n";

// TODO: import ActionSelect
const DropDownFake = (props) => <p>UTMB MONT-BLANC V</p>;
const DropDownFake2 = (props) => <p>EN V</p>;

const Menu = (props) => {
  const { children = null } = props;
  const t = useMessageFormatter(CONTENT);

  return (
    <>
      <Row as="nav" className="nav-top main-between">
        <Row>
          <Link href="/">{t("logo")}</Link>
          <Separator.V />
          <DropDownFake />
        </Row>

        <Row>
          <DropDownFake2 />
          <Separator.V />
          <Link href="/">{t("cta")} O_o</Link>
        </Row>
      </Row>
    </>
  );
};

export { Menu as TopNav };
