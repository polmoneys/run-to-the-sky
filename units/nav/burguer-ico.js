const Ico = ({ status, fillOff = "currentColor", fillOn = "transparent" }) => (
  <svg viewBox={"0 -30 100 100"} width="100" height="100">
    <rect fill={status ? fillOn : fillOff} x="0" y="0" width="60" height="8" />
    <rect fill={status ? fillOn : fillOff} x="0" y="14" width="60" height="8" />
    <rect x="0" y="28" width="60" height="8" fill={status ? fillOn : fillOff} />
  </svg>
);

export { Ico };
