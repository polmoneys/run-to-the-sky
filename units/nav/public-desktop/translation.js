export const CONTENT = Object.freeze({
  "en-EN": {
    a: "Discover",
    b: "Races & Runners",
    c: "Get Involved",
    d: "Get Excited",
    e: "E-shop",
    f: "UTMB© Live",
  },
  "en-US": {
    a: "Discover",
    b: "Races & Runners",
    c: "Get Involved",
    d: "Get Excited",
    e: "E-shop",
    f: "UTMB© Live",
  },
  "fr-FR": {
    a: "Discover",
    b: "Races & Runners",
    c: "Get Involved",
    d: "Get Excited",
    e: "E-shop",
    f: "UTMB© Live",
  },
  "es-ES": {
    a: "Discover",
    b: "Races & Runners",
    c: "Get Involved",
    d: "Get Excited",
    e: "E-shop",
    f: "UTMB© Live",
  },
});
