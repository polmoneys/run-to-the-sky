import { Pic } from "@/primitives/pic";
import { Link, Button } from "@/primitives/action";
import { Separator } from "@/primitives/separator";
import { Row, Column } from "@/primitives/box";
import { CONTENT } from "./translation";
import { useMessageFormatter } from "@react-aria/i18n";

// TODO: ACTIVELINK
const ActiveLink = ({ children, href, className }) => {
  const router = useRouter();
  return (
    <Link href={href} scroll={false}>
      <a
        className={`${
          router.pathname === href
            ? "text-gray-900 border-gray-800"
            : "text-gray-600 hover:text-gray-700 border-transparent"
        } ${className} block pb-4 font-semibold text-sm sm:text-base border-b-2 focus:outline-none focus:text-gray-900 whitespace-no-wrap`}
      >
        {children}
      </a>
    </Link>
  );
};

const Menu = (props) => {
  const { children = null } = props;
  return (
    <>
      <Row as="nav" className="nav-desktop">
        <Link href="/">
          <Pic.Logo />
        </Link>
        <Column>
          <LinksRow {...props} />
          <ProgressBar />
          {children && <Row className="self-stretch">{children}</Row>}
        </Column>
      </Row>
    </>
  );
};

const LinksRow = (props) => {
  const t = useMessageFormatter(CONTENT);

  return (
    <Row className="self-stretch">
      <Link href="/discover">{t("a")}</Link>
      <Link href="/races-and-runners">{t("b")}</Link>
      <Link href="/get-involved">{t("c")}</Link>
      <Link href="/get-excited">{t("d")}</Link>
      <Link href="/eshop">{t("e")}</Link>
      <Link href="/live">{t("f")}</Link>
      <Button>Search</Button>
    </Row>
  );
};

const ProgressBar = () => {
  // TODO: useRouter hook + some <Paint/> or useMeter
  return null;
};

const LandingMenu = (props) => {
  return (
    <Row as="nav" className="nav-desktop">
      <Link href="/">
        <Pic />
      </Link>
      <Column>
        <LinksRow {...props} />
        <Separator />
      </Column>
    </Row>
  );
};

Menu.Landing = (props) => <LandingMenu {...props} />;

export { Menu as DesktopNav };
