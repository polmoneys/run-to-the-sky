import { signin, signout, useSession } from "next-auth/client";

/**
 * The approach used in this component shows how to built a sign in and sign out
 * component that works on pages which support both client and server side
 * rendering, and avoids any flash incorrect content on initial page load.
 **/
const Nav = () => {
  const [session, loading] = useSession();
  return (
    <>
      {" "}
      {!session && (
        <>
          <span>Not signed in</span>
          <a
            href={`/api/auth/signin`}
            onClick={(e) => {
              e.preventDefault();
              signin();
            }}
          >
            Sign in
          </a>
        </>
      )}
      {session && (
        <>
          <span style={{ backgroundImage: `url(${session.user.image})` }} />
          <span>
            Signed in as <strong>{session.user.email}</strong>
          </span>
          <a
            href={`/api/auth/signout`}
            onClick={(e) => {
              e.preventDefault();
              signout();
            }}
          >
            Sign out
          </a>
        </>
      )}
    </>
  );
};
