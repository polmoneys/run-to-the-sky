import { CONTENT } from "./translation";
import { useLocale } from "@react-aria/i18n";
import { useMessageFormatter } from "@react-aria/i18n";
import { Tabs, TabHeader, TabItem } from "../../primitives/tabs";

//TODO: TOP TABS WITH SUBTABS AND TOGGLABLE LEGEND ON THE LEFT

const Statistics = (props) => {
  const { locale, direction } = useLocale();

  const t = useMessageFormatter(CONTENT);

  return (
    <>
      <p>stats</p>
      <Tabs.V
        header={({ focusedIndex, selectedIndex }) => {
          return (
            <>
              <TabHeader>Uno</TabHeader>
              <TabHeader>Dos</TabHeader>
              <TabHeader>Tres</TabHeader>
            </>
          );
        }}
      >
        <TabItem>1</TabItem>
        <TabItem>2</TabItem>
        <TabItem>3</TabItem>
      </Tabs.V>
    </>
  );
};

export { Statistics };
