import { useMessageFormatter } from "@react-aria/i18n";
import { CONTENT } from "./translation";

function MaintenanceMode() {
  const t = useMessageFormatter(CONTENT);
  return (
    <div className="maintenance">
      <p>{t("message")}</p>
    </div>
  );
}

export { MaintenanceMode };
