export const CONTENT = Object.freeze({
  "en-EN": {
    message: "Website maintenance. We'll be back online shortly.",
  },
  "en-US": {
    message: "Website maintenance. We'll be back online shortly.",
  },
  "fr-FR": {
    message: "Site en maintenance.",
  },
});
