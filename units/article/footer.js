import { Text } from "@/primitives/text";

const F = (props) => {
  return (
    <div className="article-footer">
      <p>Article footer</p>
      <Text.Money value={125000} currency="USD" />
      <Text.Money value={125000} currency="EUR" />
      <Text.Distance value={60} />
    </div>
  );
};

export { F };
