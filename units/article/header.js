import { Pic } from "@/units/pic";
import { useDateFormatter } from "@react-aria/i18n";

const H = (props) => {
  const formatter = useDateFormatter();

  return (
    <div className="article-header">
      <p>Article header</p>
      <p>{formatter.format(new Date())}</p>
    </div>
  );
};

export { H };
