import { useMessageFormatter } from "@react-aria/i18n";
import { CONTENT } from "./translation";
import { H as Header } from "./header";
import { F as Footer } from "./footer";
import { B as Body } from "./body";

const A = (props) => {
  const t = useMessageFormatter(CONTENT);

  return (
    <article>
      <h1>{t("title")}</h1>
      <Header />
      <Body />
      <Footer />
    </article>
  );
};

export { A as Article };
