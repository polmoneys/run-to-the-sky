export const CONTENT = Object.freeze({
  "en-US": {
    title: "Article title in english",
  },
  "en-EN": {
    title: "Article title in english",
  },
  "fr-FR": {
    title: "Article title en français",
  },
});
