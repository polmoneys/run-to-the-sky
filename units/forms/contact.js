import {
  FormDebug,
  Form,
  Input,
  CheckBox,
  RadioGroup,
  Radio,
  Switch,
  Textarea,
  Spacer,
  useForm,
} from "@/primitives/forms";
import { Button } from "@/primitives/action";
import { CONTENT } from "@/translations/forms";

const initialState = {
  liveValidation: true,
};
function Contact() {
  const [formStates, formActions] = useForm(() => initialState);
  const [title, SetTitle] = useState("");
  const [email, SetEmail] = useState("");
  const [featured, SetFeatured] = useState(false);
  const [status, SetStatus] = useState("draft");
  const [agree, SetAgree] = useState(false);
  const [question, SetQuestion] = useState("");

  const ops = {
    title: SetTitle,
    email: SetEmail,
    featured: SetFeatured,
    status: SetStatus,
    agree: SetAgree,
    question: SetQuestion,
  };

  const { awaiting, fail, ok, fixes } = formStates;
  const { showFixes, setError, setSuccess, setAwaiting } = formActions;

  const update = (e) => {
    const { target, persist } = e;
    if (persist) e.persist();
    const { name, value } = target;
    ops[name](value);
  };

  const save = () => {
    setAwaiting(true);
    const upcoming = {
      title,
      email,
      featured,
      status,
      agree,
      question,
    };
    console.log(upcoming);
    const valid = false;
    if (valid) {
      // api call and states
    } else {
      showFixes(true);
    }
  };

  if (fail) return <p> Fail</p>;
  if (ok) return <p> OK</p>;
  return (
    <>
      <FormDebug
        state={{
          title,
          email,
          featured,
          status,
          agree,
          question,
        }}
      />
      <Spacer />
      <Form id="test-id" onSubmit={save}>
        <Input.Email
          required
          initial={email}
          name="email"
          label="Email"
          onChange={update}
          showFixes={fixes}
          enterkeyhint="next"
        />

        <RadioGroup
          name="status"
          initial={status}
          onChange={update}
          labelGroup="Post status"
        >
          <Radio name="draft" label="Draft" />
          <Radio name="live" label="Published" />
          <Radio name="unknown" label="Unknown" />
        </RadioGroup>

        <Textarea
          initial={question}
          name="question"
          label="Question"
          onChange={update}
          showFixes={fixes}
          enterkeyhint="done"
        />
        <Spacer />
        <CheckBox
          initial={agree}
          name="agree"
          label="*"
          onChange={update}
          required
        />

        <button type="button" disabled={awaiting} onClick={save}>
          Send
        </button>
      </Form>
    </>
  );
}

export { Contact };
