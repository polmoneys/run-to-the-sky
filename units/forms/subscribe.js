import {
  FormDebug,
  Form,
  Input,
  CheckBox,
  RadioGroup,
  Radio,
  Switch,
  Textarea,
  Spacer,
  useForm,
} from "@/primitives/forms";
import { Button } from "@/primitives/action";
import { CONTENT } from "@/translations/forms";

const initialState = {
  liveValidation: true,
};

function Subscribe() {
  const [formStates, formActions] = useForm(() => initialState);
  const [email, SetEmail] = useState("");
  const [agree, SetAgree] = useState(false);

  const ops = {
    email: SetEmail,
    agree: SetAgree,
  };

  const { awaiting, fail, ok, fixes } = formStates;
  const { showFixes, setError, setSuccess, setAwaiting } = formActions;

  const update = (e) => {
    const { target, persist } = e;
    if (persist) e.persist();
    const { name, value } = target;
    ops[name](value);
  };

  const save = () => {
    setAwaiting(true);
    const upcoming = {
      email,
      agree,
    };
    console.log(upcoming);
    const valid = false;
    if (valid) {
      // api call and states
    } else {
      showFixes(true);
    }
  };

  if (fail) return <p> Fail</p>;
  if (ok) return <p> OK</p>;
  return (
    <>
      <FormDebug
        state={{
          email,
          agree,
        }}
      />
      <Spacer />
      <Form id="test-id" onSubmit={save}>
        <Input.Email
          required
          initial={email}
          name="email"
          label="Email"
          onChange={update}
          showFixes={fixes}
          enterkeyhint="next"
        />

        <Spacer />
        <CheckBox
          initial={agree}
          name="agree"
          label="*"
          onChange={update}
          required
        />

        <button type="button" disabled={awaiting} onClick={save}>
          Subscribe
        </button>
      </Form>
    </>
  );
}

export { Subscribe };
