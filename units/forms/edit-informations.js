import {
  FormDebug,
  Form,
  Input,
  CheckBox,
  RadioGroup,
  Radio,
  Switch,
  Textarea,
  Spacer,
  useForm,
  Select,
  Item,
} from "@/primitives/forms";
import { Button } from "@/primitives/action";
import { CONTENT } from "@/translations/forms"; // TODO: TRANSLATE <LABEL/>S

const CancelButton = (props) => (
  <Button className="-white _darkgrey" onPress={props.cb}>
    CANCEL
  </Button>
);
const SaveButton = (props) => (
  <Button className="-white _blue" disabled={props.awaiting} onPress={props.cb}>
    SAVE CHANGES
  </Button>
);

const initialState = {
  liveValidation: true,
};
function EditInformations() {
  const [formStates, formActions] = useForm(() => initialState);
  const [lastName, SetLastName] = useState("");
  const [firstName, SetFirstName] = useState("");
  const [dob, SetDateOfBirth] = useState("");
  const [nationality, SetNationality] = useState(undefined);
  const [spoken, SetSpokenLanguages] = useState(
    { lang: "english", level: "fluent" },
    { lang: "french", level: "native" },
  ); // TODO: IN WHAT LANG ARE WE SAVING THIS DATA ? LEVEL ¿?
  const [gender, SetGender] = useState("male"); // TODO: IN WHAT LANG ARE WE SAVING THIS DATA ? LEVEL ¿?
  const [tshirtSize, SetTshirtSize] = useState("m");
  const [selectedKeys, setSelectedKeys] = useState("");

  const ops = {
    lastName: SetLastName,
    firstName: SetFirstName,
    dob: SetDateOfBirth,
    nationality: SetNationality,
    spoken: SetSpokenLanguages,
    gender: SetGender,
    tshirtSize: SetTshirtSize,
  };

  const { awaiting, fail, ok, fixes } = formStates;
  const { showFixes, setError, setSuccess, setAwaiting } = formActions;

  const update = (e) => {
    const { target, persist } = e;
    if (persist) e.persist();
    const { name, value } = target;
    ops[name](value);
  };

  const save = () => {
    setAwaiting(true);
    const upcoming = {
      lastName,
      firstName,
      dob,
      nationality,
      spoken,
      gender,
      tshirtSize,
    };
    console.log(upcoming);
    const valid = false;
    if (valid) {
      // api call and states
    } else {
      showFixes(true);
    }
  };

  const cancel = () => {
    // TODO: router push back to dashboard/informations ?¿
  };

  if (fail) return <p> Fail</p>;
  if (ok) return <p> OK</p>;
  return (
    <>
      <FormDebug
        state={{
          lastName,
          firstName,
          dob,
          nationality,
          spoken,
          gender,
          tshirtSize,
          selectedKeys,
        }}
      />
      <Spacer />
      <Form id="test-id" onSubmit={save}>
        <Input
          required
          initial={lastName}
          name="lastName"
          label="LAST NAME"
          onChange={update}
          showFixes={fixes}
          enterkeyhint="next"
        />
        <Input
          required
          initial={firstName}
          name="firstName"
          label="FIRST NAME"
          onChange={update}
          showFixes={fixes}
          enterkeyhint="next"
        />

        <Input
          required
          initial={dob}
          name="dob"
          label="DATE OF BIRTH"
          onChange={update}
          showFixes={fixes}
          enterkeyhint="next"
        />
        {/* MENU GROUP 
              <Input
          required
          initial={nationality}
          name="nationality"
          label="NATIONALITY"
          onChange={update}
          showFixes={fixes}
          enterkeyhint="next"
        />
        
        */}

        <RadioGroup
          name="gender"
          initial={gender}
          onChange={update}
          labelGroup="GENDER"
        >
          <Radio name="male" label="MALE" />
          <Radio name="female" label="FEMALE" />
          <Radio name="undisclosed" label="I'd rather not say" />
        </RadioGroup>

        {/* DIY FORM SPOKEN LANGUAGES */}
        <Spacer />

        <RadioGroup
          name="tshirtSize"
          initial={tshirtSize}
          onChange={update}
          labelGroup="TSHIRT SIZE"
        >
          <Radio name="s" label="S - Unisex" />
          <Radio name="m" label="M - Unisex" />
          <Radio name="l" label="L - Unisex" />
          <Radio name="xl" label="XL - Unisex" />
        </RadioGroup>

        <div>
          <Select
            selectedKey={selectedKeys}
            onSelectionChange={setSelectedKeys}
            label="TSHIRT SIZE"
            name="tshirt"
          >
            <Item key={"red"}>Red</Item>
            <Item key={"orange"}>Orange</Item>
            <Item key={"yellow"}>Yellow</Item>
            <Item key={"green"}>Green</Item>
            <Item key={"blue"}>Blue</Item>
            <Item key={"purple"}>Purple</Item>
          </Select>
        </div>

        <CancelButton cb={cancel} />
        <SaveButton cb={save} />
      </Form>
    </>
  );
}

export { EditInformations };
