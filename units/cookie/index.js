import { useEffect, useState } from "react";
import { useLocalStorage } from "@/utils/use-local-storage";
import { CONTENT } from "./translation";
import { useMessageFormatter } from "@react-aria/i18n";

// TODO : DYNAMIC IMPORT SSR:FALSE

function Cookie({ storageKey = "gdrp", name = "jane" }) {
  const [dissmised, setStatus] = useLocalStorage(storageKey, "N");
  const t = useMessageFormatter(CONTENT);

  const close = () => {
    setStatus(false);
    setStatus("Y");
  };

  if (dissmised === "Y") return null;
  return (
    <div id="cookie">
      <button type="button" onClick={close} className="h2">
        &times;
      </button>
      <h1>{t("greeting", { name })}</h1>
    </div>
  );
}

export { Cookie };
