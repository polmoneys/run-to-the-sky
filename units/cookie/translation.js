export const CONTENT = Object.freeze({
  "en-US": {
    greeting: "Hello, {name}!",
  },
  "en-EN": {
    greeting: "Hello, {name}!",
  },
  "fr-FR": {
    greeting: "Bonjour, {name}!",
  },
});
