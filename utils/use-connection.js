import { useEffect, useState } from "react";
import { useIsSSR } from "@react-aria/ssr";

const isSlowSpeed = (connection) =>
  (connection && connection.saveData) ||
  ["slow-2g", "2g", "3g"].includes(connection.effectiveType);

function useUserConnection() {
  const [speed, setSpeed] = useState("slow");

  const updateNetwork = () => {
    const connection =
      navigator.connection ||
      navigator.mozConnection ||
      navigator.webkitConnection;
    if (!connection) return; // defaults to 'slow'
    if (isSlowSpeed(connection)) {
      setSpeed("slow");
    } else {
      setSpeed("ok");
    }
  };

  useEffect(() => {
    navigator.connection.addEventListener("change", updateNetwork);
    return () => {
      navigator.connection.removeEventListener("change", updateNetwork);
    };
  });
  return speed;
}
