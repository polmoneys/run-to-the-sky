/**
 * Pol Moneys 2020
 * 1.0.0
 * 🍢 🍡
 *
 * USAGE https://codesandbox.io/s/usecollection-z0zck?file=/src/App.js
 */

import { useRef, useState } from "react";

const useCollection = (initialState = []) => {
  const collection = useRef(new Map(initialState));
  const [xs, setXS] = useState(() => Array.from(initialState));

  const log = () => {
    for (const [key, value] of collection.current) {
      // entries, keys, values
      console.log(`${key}: ${value}`);
    }
  };

  const update = () => {
    log();
    const next = Array.from(collection.current);
    console.log("next", next);

    setXS(next);
  };

  const empty = () => {
    collection.current.clear();
    update();
  };

  const add = (key, value) => {
    collection.current.set(key, value);
    update();
  };

  const addItem = (key, value) => () => {
    // curried for onClick={addItem(item)}
    collection.current.set(key, value);
    update();
  };

  const remove = (key) => {
    collection.current.delete(key);
    update();
  };

  const removeItem = (key) => () => {
    collection.current.delete(key);
    update();
  };

  const has = (key) => collection.current.has(key);

  return [
    { xs, collection },
    {
      empty,
      add,
      addItem,
      remove,
      removeItem,
      has,
    },
  ];
};

export { useCollection };
