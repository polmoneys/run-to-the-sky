import { apiCall, apiCall2 } from "./api";

const ToolBar = () => {
  const toast = useToast();

  const withErrorHandling = (callback) => async () => {
    try {
      await callback();
    } catch (e) {
      toast("ups", e.message, "error");
    }
  };

  return (
    <>
      <button onClick={withErrorHandling(apiCall)}>1</button>
      <button onClick={withErrorHandling(apiCall2)}>2</button>
    </>
  );
};
