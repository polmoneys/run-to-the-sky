import { useEffect, useState } from "react";

// Use only in dynamic imports with ssr:false

function getLocalStorageValue(key) {
  const val = localStorage.getItem(key);
  if (!val) return null;
  try {
    return JSON.parse(val);
  } catch (e) {
    return null;
  }
}

function setLocalStorage(key, value) {
  localStorage.setItem(key, JSON.stringify(value));
}

function useLocalStorage(key, defaultState) {
  const [state, setState] = useState(getLocalStorageValue(key) || defaultState);

  useEffect(() => {
    setLocalStorage(key, state);
  }); // FIXME: ,[] SO THAT ONLY RUNS ONCE ?¿

  return [state, setState];
}

export { useLocalStorage };
