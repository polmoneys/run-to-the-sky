import { animate, timeline } from "./animations";
export { animate, timeline };

export const uniqueId = () => Math.random().toString(36).substring(2, 15);

export const isApiSupported = (api) => api in window;
export const prefersNoAnimations = () =>
  window.matchMedia && matchMedia("(prefers-reduced-motion)").matches;

const isIE11 = () => !!window.MSInputMethodContext && !!document.documentMode;

// eslint-disable-next-line no-self-compare
const isNaN = Number.isNaN || ((value) => value !== value);

function isBoolean(x) {
  return typeof x === "boolean";
}

export const boolOrBoolString = (x) =>
  x === "true" ? true : isBoolean(x) ? x : false;

// NOOPS

export const cbOrNoop = (fn) => {
  return typeof fn === "function" ? fn : noop;
};

export const noop = () => ({});
export const noopStr = () => "";

export const cbIf = (condition, fnTrue, fnFalse) =>
  condition ? fnTrue() : fnFalse();

function preventZoomSafari(el) {
  // Safari on iOS >= 10 to zoom the page
  const cb = (e) => e.preventDefault();
  el.addEventListener("gesturestart", cb);
  el.addEventListener("gesturechange", cb);
}

function blurNode(reactNode) {
  if (reactNode && reactNode.blur) {
    reactNode.blur();
  }
}

function focusNode(reactNode) {
  if (reactNode && reactNode.focus) {
    reactNode.focus();
  }
}

// XS

export const XSidify = (xs) =>
  xs
    .map((i, ix) => ({ ...i, id: Number(ix + 1) }))
    .sort((a, b) => {
      return (a["id"] < b["id"] ? -1 : 1) * 1;
    });

export const isXS = (target) => Array.isArray(target);

export const isXSEmpty = (xs) => xs.length < 1;

export const compactXS = (xs) =>
  xs.filter((x) => x !== null && x !== undefined);

export const unwrapXS = (arg) => (Array.isArray(arg) ? arg[0] : arg);

export const paginate = (xs, size) =>
  Array.from({ length: Math.ceil(xs.length / size) }, (v, i) =>
    xs.slice(i * size, i * size + size),
  );

export const xsToObjbyId = (xs) =>
  xs.reduce((hash, { id, ...rest }) => {
    hash[id] = { ...rest };
    return hash;
  }, {});

export const XSMatch = (xs, fn) => {
  let results = [];
  for (const value of xs) fn(value) ? results.push(value) : undefined;
  return results;
};

export const XSSplit = (xs, fn) => {
  let match = [];
  let dispose = [];
  for (const el of xs) {
    if (fn(el) === true) {
      match.push(el);
    } else {
      dispose.push(el);
    }
  }
  return [match, dispose];
};

export const XSSortBy = (xs, key, sortOrder) => {
  if (!key) return;
  const sorted = xs.sort((a, b) => {
    return (a[key] < b[key] ? -1 : 1) * (sortOrder === "isAsc" ? 1 : -1);
  });

  return sorted;
};

export const isNotHead = (items) => items.length !== 1;
export const isTail = (items, id) => items.length === id;

// OBJ

export const isObjEmpty = (obj) => compactObj(obj).keys < 1;

export const compactObj = (obj) => {
  const result = {};
  Object.keys(obj).forEach((key) => {
    if (obj[key] !== undefined) {
      result[key] = obj[key];
    }
  });
  return result;
};

export const objValuesToXS = (obj) => {
  let xs = [];
  Object.keys(obj).map((key) => {
    let item = obj[key];
    if (typeof item === "object") {
      xs = xs.concat(toArray(item));
    } else {
      xs.push(item);
    }
  });
  return xs;
};
