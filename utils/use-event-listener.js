import { useCallback, useEffect } from "react";

/*

USAGE
-----

useEventListener('mousemove', handler);
const handler = useCallback(
    ({ clientX, clientY }) => {
    setStateX({ x: clientX, y: clientY });
    },
    [stateX]
);

 */

function useEventListener(eventName, handler, element = window) {
  const savedHandler = useRef();
  useEffect(() => {
    savedHandler.current = handler;
  }, [handler]);

  useEffect(() => {
    const eventListener = (event) => savedHandler.current(event);
    element.addEventListener(eventName, eventListener);

    return () => {
      element.removeEventListener(eventName, eventListener);
    };
  }, [eventName, element]);
}

export { useEventListener };
