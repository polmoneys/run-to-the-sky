import { useEffect, useState } from "react";
import { useIsSSR } from "@react-aria/ssr";

const visualViewport = typeof window !== "undefined" && window.visualViewport;

export function useViewportSize() {
  const [size, setSize] = useState(() => getViewportSize());

  useEffect(() => {
    // Use visualViewport api to track available height even on iOS virtual keyboard opening
    let onResize = () => {
      setSize(getViewportSize());
    };

    if (!visualViewport) {
      window.addEventListener("resize", onResize);
    } else {
      visualViewport.addEventListener("resize", onResize);
    }

    return () => {
      if (!visualViewport) {
        window.removeEventListener("resize", onResize);
      } else {
        visualViewport.removeEventListener("resize", onResize);
      }
    };
  }, []);

  const isSSR = useIsSSR();
  return isSSR ? false : size;
}

function getViewportSize() {
  return {
    width: visualViewport?.width || window.innerWidth,
    height: visualViewport?.height || window.innerHeight,
  };
}
