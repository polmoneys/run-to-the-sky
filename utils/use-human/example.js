import useSWR from "swr";
import fetcher from "../fetcher-swr";

function useUser(id) {
  const { data, error } = useSWR(`/api/user/${id}`, fetcher, {
    onErrorRetry: (error, key, config, revalidate, { retryCount }) => {
      // Never retry on 404.
      if (error.status === 404) return;
      // Never retry for a specific key.
      if (key === "/api/user") return;
      // Only retry up to 10 times.
      if (retryCount >= 10) return;
      // Retry after 5 seconds.
      setTimeout(() => revalidate({ retryCount: retryCount + 1 }), 5000);
    },
  });
  return {
    user: data,
    isLoading: !error && !data,
    isError: error,
  };
}
