import { createContext, useContext } from "react";
/*
USAGE
-----
const Human = () => {
  const firstName = useFirstName();
  const lastName = useLastName();
  const stat = useStat();
  return (
    <div>
      <p>
        {firstName} {lastName}
      </p>
      <p>{stat}</p>
    </div>
  );
};

*/

const AuthContext = createContext();

const AuthProvider = ({ children }) => {
  // TODO: FETCH
  const makeHuman = () => ({
    firstName,
    lastName,
    stat,
    statb,
    statc,
  });
  return (
    <AuthContext.Provider
      value={{
        ...makeHuman(),
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

function useHuman() {
  const context = useContext(AuthContext);
  if (context === undefined) {
    throw new Error(`useHuman must be used within an AuthProvider`);
  }
  return context;
}

export { AuthProvider, useHuman };

const useFirstName = () => {
  const person = useHuman();
  return person.firstName;
};
const useLastName = () => {
  const person = useHuman();
  return person.lastName;
};
const useStat = () => {
  const person = useHuman();
  return person.stat;
};

export { useFirstName, useLastName, useStat };
