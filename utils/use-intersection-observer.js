import { useEffect, useRef, useState } from "react";
import IntersectionObserver from "intersection-observer";

// TODO: Orchestrated https://github.com/alphagov/govuk-design-system/blob/master/src/javascripts/components/back-to-top.js

// Use only in dynamic imports with ssr:false

const useIntersectionObserver = (options) => {
  const [elements, setElements] = useState([]);
  const [entries, setEntries] = useState([]);
  const observer = useRef(null);
  const { root, rootMargin, threshold } = options || {};
  useEffect(() => {
    if (elements.length) {
      // console.log("=>>>> CONNECTING");
      observer.current = new IntersectionObserver(
        (ioEntries) => {
          setEntries(ioEntries);
        },
        {
          threshold,
          root,
          rootMargin,
        },
      );
      elements.forEach((element) => {
        observer.current.observe(element);
      });
    }
    return () => {
      if (observer.current) {
        // console.log("=>>>> DISCONNECTING");
        observer.current.disconnect();
      }
    };
  }, [elements, root, rootMargin, threshold]);
  return [observer.current, setElements, entries];
};

export { useIntersectionObserver };
