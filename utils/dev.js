import { isValidElement } from "react";

function perf(fn) {
  console.time("fn");
  const a1 = performance.now();
  fn();
  console.timeEnd("fn");
  const a2 = performance.now();
  return a2 - a1;
}

function randomBool() {
  return Math.random() < 0.5;
}

const uniqueId = () => Math.random().toString(36).substring(2, 15);

function isEquivalentObj(a, b) {
  const aProps = Object.getOwnPropertyNames(a);
  const bProps = Object.getOwnPropertyNames(b);
  if (aProps.length != bProps.length) {
    return false;
  }

  for (var i = 0; i < aProps.length; i++) {
    var propName = aProps[i];
    if (a[propName] !== b[propName]) {
      // If values of same property are not equal,
      // objects are not equivalent
      return false;
    }
  }
  return true;
}

const shallowDiff = (a, b) => {
  for (let i in a) {
    if (!(i in b)) return true;
  }

  for (let i in b) {
    if (a[i] !== b[i]) return true;
  }

  return false;
};

const canUseDOM = typeof window !== "undefined";

export function formatTimeAsCups(minutes) {
  const cups = Math.round(minutes / 5);
  return `${new Array(cups || 1).fill("☕️").join("")} ${minutes} min process`;
}

function isFunctionComponent(component) {
  return typeof component === "function";
}

const isReactElement = (value) =>
  isFunctionComponent(value) || isValidElement(value);

export {
  isReactElement,
  isFunctionComponent,
  isEquivalentObj,
  perf,
  randomBool,
  uniqueId,
};
