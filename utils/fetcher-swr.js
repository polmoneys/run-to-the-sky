/*
USAGE
-----

const { data, error } = useSWR('/api/user', fetcher)
// error.info === {
//   message: "You are not authorized to access this resource.",
//   documentation_url: "..."
// }
// error.status === 403


// conditionally fetch
const { data } = useSWR(shouldFetch ? '/api/data' : null, fetcher)
// ...or return a falsy value
const { data } = useSWR(() => shouldFetch ? '/api/data' : null, fetcher)

*/
const fetcher = async (url) => {
  const res = await fetch(url);
  // If the status code is not in the range 200-299,
  // we still try to parse and throw it.
  if (!res.ok) {
    const error = new Error("An error occurred while fetching the data.");
    // Attach extra info to the error object.
    error.info = await res.json();
    error.status = res.status;
    console.log("o_O", res);
    throw error;
  }
  return await res.json();
};

export default fetcher;

// MUTATE LOGOUT/POST https://codesandbox.io/s/swr-auth-tuqtf?from-embed and https://swr.vercel.app/docs/mutation
// SWR OPTS https://swr.vercel.app/docs/options#options
// SWR GLOBAL CONFIG https://swr.vercel.app/docs/global-configuration
