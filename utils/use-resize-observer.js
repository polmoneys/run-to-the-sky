import { useEffect, useState, useRef } from "react";
import ResizeObserver from "resize-observer-polyfill";
import { useIsSSR } from "@react-aria/ssr";

function useResizeObserver(targetRef) {
  const [contentRect, setContentRect] = useState({});
  const resizeObserver = useRef(null);

  useEffect(() => {
    observe(ResizeObserver);

    function observe(ResizeObserver) {
      resizeObserver.current = new ResizeObserver((entries) => {
        const {
          width,
          height,
          top,
          right,
          bottom,
          left,
        } = entries[0].contentRect;
        setContentRect({ width, height, top, right, bottom, left });
      });
      if (targetRef.current) {
        resizeObserver.current.observe(targetRef.current);
      }
    }

    return disconnect;
  }, [targetRef]);

  function disconnect() {
    if (resizeObserver.current) {
      resizeObserver.current.disconnect();
    }
  }
  const isSSR = useIsSSR();
  return isSSR ? false : contentRect;
}

export { useResizeObserver };
