import { useEffect, useLayoutEffect, useState } from "react";
import { useIsSSR } from "@react-aria/ssr";

function useMediaQuery(query) {
  const supportsMatchMedia =
    typeof window !== "undefined" && typeof window.matchMedia === "function";
  const [matches, setMatches] = useState(() =>
    supportsMatchMedia ? window.matchMedia(query).matches : false,
  );

  useEffect(() => {
    if (!supportsMatchMedia) {
      return;
    }

    let mq = window.matchMedia(query);
    let onChange = (evt) => {
      setMatches(evt.matches);
    };

    mq.addEventListener(onChange);
    return () => {
      mq.removeEventListener(onChange);
    };
  }, [supportsMatchMedia, query]);

  const isSSR = useIsSSR();
  return isSSR ? false : matches;
}

export { useMediaQuery };
