/**
 * React Animations 🎡 🎢 🎠
 * @polmoneys  #2020#
 * version 1.0.0
 * port of this fab post https://dassur.ma/things/raf-promise/
 */

/* eslint-disable*/

function transitionEndPromise(element, prop) {
  return new Promise((resolve) => {
    const node = element.current;
    node.addEventListener("transitionend", function f(event) {
      if (event.target !== node) return;
      node.removeEventListener("transitionend", f);
      resolve();
    });
  });
}

function timeline() {
  return new Promise((resolve) => requestAnimationFrame(resolve));
}

function animate(element, stylz, prop) {
  Object.assign(element.current.style, stylz);
  return transitionEndPromise(element, prop).then(() => timeline());
}

export { animate, timeline };
