import { useCallback, useState } from "react";

/*
EXAMPLE USAGE: https://codesandbox.io/s/usestatus-wsg46
*/

function useBoolean(initialState = false) {
  const [status, setStatus] = useState(initialState);
  const toggle = useCallback(() => setStatus((state) => !state), []);
  return [status, toggle];
}

export { useBoolean };
