import { DEFAULT_THEME, THEMES } from "./theme";
export { DEFAULT_THEME, THEMES };

export const NAVIGATION_BREAKPOINT = 480; // 🚨  sync with nav.css

export const IMAGE_PLACEHOLDER =
  "https://via.placeholder.com/1600x900/444444/f9f9f9/?text=PIC+SAMPLE";
export const PLACEHOLDER_SRC = `data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D`;

export const FAVICON_IMAGE_PATH = "/favicon.png";
export const PROFILE_IMAGE_PATH = "/profile.jpg";

// TIME
export const SECOND = 1000;
export const MINUTE = 60 * SECOND;
export const HOUR = 60 * MINUTE;
export const DAY = 24 * HOUR;

export const DEFAULT_MAX_AGE = 30 * DAY;

export const STEPS_FLOW = Object.freeze({
  idle: 0,
  awaiting: 1,
  completed: 2,
  error: 3,
});

// RESPONSE CODES
export const OK = 200;
export const MOVED_PERMANENTLY = 301;
export const TEMPORARY_REDIRECT = 307;
export const UNAUTHORIZED = 401;
export const BAD_REQUEST = 400;
export const NOT_FOUND = 404;
export const INTERNAL_SERVER_ERROR = 500;
export const SERVICE_UNAVAILABLE = 503; // MAINTENANCE MODE

//  HEADERS
//  REQ https://github.com/aetheric/http-constants/blob/master/src/main/consts-headers-request.js
//  RES https://github.com/aetheric/http-constants/blob/master/src/main/consts-headers-response.js
