const SCALE_Z = Object.freeze({
  default: 1,
  top: 10,
  max: 100,
});

const COMMON_STYLES = Object.freeze({
  quiet: "transparent",
  elevation: { ...SCALE_Z },
});

const THEMES = Object.freeze({
  light: {
    defaultColor: "DimGrey",
    defaultBg: "WhiteSmoke",
    good: "coral",
    bad: "#649d66",
    ...COMMON_STYLES,
  },
  dark: {
    defaultColor: "WhiteSmoke",
    defaultBg: "DimGrey",
    good: "coral",
    bad: "#649d66",
    ...COMMON_STYLES,
  },
});

export const DEFAULT_THEME = "light";

export { THEMES };
