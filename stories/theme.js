import React from "react";

function BR() {
  return <br aria-hidden="true" />;
}

const Card = () => {
  return <div className="card" />;
};

const Circle = ({ className, size = 50, fill = "#ff5555" }) => {
  const clss = [className].filter(Boolean).join(" ");
  return (
    <div className={clss}>
      <svg
        viewBox={`-${size / 2} -${size / 2} ${size} ${size}`}
        width={size}
        height={size}
      >
        <circle fill={fill} cx={0} cy={0} r={size / 2} />
      </svg>
    </div>
  );
};

export { Circle, BR, Card };
