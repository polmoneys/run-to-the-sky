var fakeRank = 0;
const OF = [
  { rank: ++fakeRank, team: "AAA", fastlap: 280, average: 240 },
  { rank: ++fakeRank, team: "AAA", fastlap: 290, average: 230 },
  { rank: ++fakeRank, team: "AAA", fastlap: 300, average: 220 },
  { rank: ++fakeRank, team: "AAA", fastlap: 230, average: 225 },
  { rank: ++fakeRank, team: "BBB", fastlap: 220, average: 224 },
  { rank: ++fakeRank, team: "DDD", fastlap: 240, average: 219 },
  { rank: ++fakeRank, team: "BBB", fastlap: 250, average: 200 },
  { rank: ++fakeRank, team: "BBB", fastlap: 260, average: 220 },
  { rank: ++fakeRank, team: "BBB", fastlap: 270, average: 230 },
  { rank: ++fakeRank, team: "AAA", fastlap: 277, average: 233 },
  { rank: ++fakeRank, team: "CCC", fastlap: 243, average: 222 },
  { rank: ++fakeRank, team: "CCC", fastlap: 250, average: 214 },
  { rank: ++fakeRank, team: "CCC", fastlap: 256, average: 211 },
  { rank: ++fakeRank, team: "DDD", fastlap: 220, average: 215 },
];
export { OF };
