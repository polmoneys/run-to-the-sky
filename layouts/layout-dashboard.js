import dynamic from "next/dynamic";
import Head from "next/head";
import Link from "next/link";
// import { DesktopNav } from '@/units/private-desktop';
// import { mobileNav } from '@/units/private-mobile';
import { Footer } from "@/units/footer";

const Layout = ({ children, locale, title = "UTMB" }) => (
  <>
    <Head>
      <title>{title}</title>
    </Head>
    {/* <DesktopNav/> */}
    {children}
    {/* <Footer /> */}
  </>
);

export { Layout };
