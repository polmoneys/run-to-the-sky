import dynamic from "next/dynamic";
import Head from "next/head";
import Link from "next/link";
import { UnsupportedBrowserMessage } from "@/units/unsupported"; // TODO: THIS NEEDS TO BE OUTSIDE REACT
// import { DesktopNav } from '@/units/public-desktop';
// import { mobileNav } from '@/units/public-mobile';
import { Footer } from "@/units/footer";

const SafeSSRCoookie = dynamic(
  () => import("@/units/cookie/index.js").then((mod) => mod.Cookie),
  {
    ssr: false,
  },
);

const Layout = ({ children, locale, title = "UTMB" }) => (
  <>
    <Head>
      <title>{title}</title>
    </Head>
    {/* <DesktopNav/> */}
    {/* <UnsupportedBrowserMessage /> */}
    {/* <SafeSSRCoookie /> */}
    {children}
    {/* <Footer /> */}
  </>
);

export { Layout };
