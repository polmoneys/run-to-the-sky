# LAYOUTS

Default `Layout` can be overriden on a `page\*\*` as in:

```javascript

import {Layout} from '@/layouts/***';

const PageName = (props ) => {

    ...
    ...
    ...

}

PageName.Layout = Layout;

```

[PERSISTANCE](https://dev.to/ozanbolel/layout-persistence-in-next-js-107g)
