import { rest } from "msw";
import runners from './runners';
import users from './users';
import { unwrapXS } from "@/utils";

export const handlers = [
  
  rest.get("https://x.com/runners", (req, res, ctx) => {
    return res(ctx.json(runners));
  }),

 rest.get('/runners/:id', (req, res, ctx) => {
    const { id } = req.params
    return res(
      ctx.json({
    ...unwrapXS(
      runners.filter((r) => r.id === id),
    )
      }),
    )
  }),
];
