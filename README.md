# UTMB

[Contribute](#contribute)

[Tips](#tips)

[Dx](#dx)

## CONTRIBUTE

The goal of all LOC is to be social/inclusive, to allow easy/less scary contributions.

One rule: keep code in the 'present', better said by Fowler:

> The additional "design ahead" code creates additional complexity, which calls for additional testing, additional defect correction, and so on. The overall effect is to slow down the project. Experts agree that the best way to prepare for future requirements is not to write speculative code; it's to make the currently required code as clear and straightforward as possible so that future programmers will know what it does and does not do and will make their changes accordingly.

We encourage AFAP, 'as flat as possible' project/files structure.

`````javascript

    src/
        context
        layouts
        mocks // msw.io 
        pages // 🔥
        primitives
        public // Assets
        styles
        stories // Storybook
        tokens // Constants
        translations
        units
        utils // Hooks

```
`primitives/*`: Like atoms you mix them and get a 'pencil' and mix again and get a 'purse'. Use them as they are enriched with a11y.

`units/*`: Here is where you can store 'pencils' and your collection of 'purses'. Favour 'name-name.js' over 'nameName.js'. Long file names are ok.

Folders are aliased at 'jsonconfig.json' so you can avoid writing the full path by prefixing with '@/':

````javascript

// **.js

import { Action } from '@/primitives/action';
import { useResizeObserver } from '@/utils';

`````

Any unstable 'file' should be sufixed with `-danger` to make it obvious that shouldn't be used in production without accepting the risk.

## NEXT.JS TIPS

Next.js data fetching can happen a couple of ways/places/moments on page basis:

```javascript
export async function getStaticProps() {
  // `getStaticProps` is invoked on the server-side,
  // so this `fetcher` function will be executed on the server-side.
  const posts = await fetcher("/api/posts");
  return { props: { posts } };
}

function Posts(props) {
  // Here the `fetcher` function will be executed on the client-side.
  const { data } = useSWR("/api/posts", fetcher, { initialData: props.posts });
  // ...
}
```

In SSR you may want to skip some component from the initial hidration, or maybe load a dependency. Embrace these patterns:

```javascript
// OPTION A

import dynamic from "next/dynamic";

const SafeSSRCountdown = dynamic(
  () => import("react-countdown").then((mod) => mod),
  {
    loading: () => <p>Loading...</p>,
    ssr: false, // MAGIC HAPPENS HERE <3
  },
);

// ...
<SafeSSRCountdown date={GOAL_DATE} />;
// ...

// OPTION B

const Post = (props) => {
  return (
    <div>
      <h1>{props.post.title}</h1>
      <p>Published on {props.date}</p>
      <p>{props.post.content}</p>
    </div>
  );
};

Post.getInitialProps = async ({ query }) => {
  const dayjs = (await import("dayjs")).default();
  return {
    date: dayjs.format("dddd D MMMM YYYY"),
    post: posts[query.id],
  };
};
```

## ISG

To do...

## 'GLOBAL' IN NEXT.JS

If you need shared components in all your pages (like a menu or a toolbar), take a look at the App component. Also you can 'hack' your way doing thing like this:

```javascript
function Index() {}

Index.isHome = true;

// _app

function MyApp({ Component, pageProps }) {
  console.log(Component.isHome);
  return <Component {...pageProps} />;
}
```

A better way to handle it would be to use `getStaticProps` (or `getServerSideProps`) on your `pages/index.js` and return that flag in props and then use `pageProps` in your `_app` to determine if the flag was set.

## CAVEATS NEXT.JS

Currently there is no way to globally hydrate data for an entire application, other than using `getInitialProps` in `_app.js` which unfortunately then disables **automatic static optimization**.

## DX

[Development](#dev)

[Deployment](#deploy)

[Onboarding](#onboarding)

### DEV

`.env.local` is where you should store secrets and be gitignored. Loaded by Next.js into `process.env` you can use them for data fecthing methods and API routes.

Run

```bash

$ npm start

# or develop components
$ npm run storybook

# share mobile localhost
$ ngrok http -auth "testUser:testPass" 3000

# prettify output as prettier.config.js
$ npm run pretty

```

You can develop against a fake api with **mock service worker** [msw docs](https://mswjs.io/). 

Branch new feature/ticket

```bash

$ git checkout -b 'feature-name'

```

Once work is complete, [merge](https://stackoverflow.com/a/47073015) back upstream.

Git can make your life [miserable](https://ohshitgit.com/) fast so don't panic. If something goes bad try this first:

```bash
$ git reflog
# you will see a list of every thing you've
# done in git, across all branches!
# each one has an index HEAD@{index}
# find the one before you broke everything
$ git reset HEAD@{index}
# magic time machine
```

Or [this](https://www.aleksandrhovhannisyan.com/blog/dev/undoing-changes-in-git/) and [that](https://sethrobertson.github.io/GitFixUm/fixup.html)

Remember that you can add more than 1 line to a commit message:

```bash

$ git commit -m 'first line' -m 'second line' -m 'third line'

```

| Commit type | EMOJI |
| :---------- | :---: |
| Fix         |  🦖   |
| New feature |  🦩   |
| Milestone   |  🦚   |
| Refractor   |  💅   |

### DEPLOY

Build

```bash

$ npm run build

# run app analytics
$ npm run analize

```

CI/CD: ...

Staging: ...

Production: ...

### ONBOARDING

Some files for you to know:

`env.local-sample.js` should store secrets and be gitignored. Rename the sample to `env.local` and ask your peers for any 'updates' that might be missing.

We may use `env.development` and `env.production` to provide environment vars to different places. You should commit these files as it defines defaults.

Clone repo from Gitlab (you may need to be granted access)

```bash

$ git clone

```

Install deps

```bash

$ npm i

```

# DOC HISTORY

First draft by [Pol](http://polmoneys.com) on the Year Of the Pandemic.
