import React from 'react';
import '../styles/critical.css';
import '../styles/theme.css';
import '../stories/styles.css';

import '../primitives/accordion/accordion.css';
import '../primitives/action/action.css';
import '../primitives/box/box.css';
import "../primitives/breadcrumb/breadcrumb.css";

import '../primitives/form/form.css';
import '../primitives/form/select/select.css';
import '../primitives/form/typeahead/typeahead.css';
import '../primitives/form/input/input.css';
import '../primitives/form/checkbox/checkbox.css';
import '../primitives/form/radio/radio.css';
import '../primitives/form/textarea/textarea.css';

import '../primitives/pic/pic.css';
import '../primitives/tabs/tabs.css';
import '../primitives/table/table.css';
import '../primitives/text/text.css';
import '../primitives/overlay/overlays.css';
import "../units/card/card.css";


export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
    previewTabs: { 'storybook/docs/panel': { index: -1 } },
}

export const decorators = [(Story) => <div style={{ padding: '0px' }}><Story/></div>];
