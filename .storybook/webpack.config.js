const path = require('path');

module.exports = ({ config }) => {
  // Add absolute path.resolve so storybook can handle absolute import (eg. @src/resources/...)
  config.resolve.alias = {
    ...config.resolve.alias,
    "@src": path.resolve(__dirname, "../"),
    "@/utils": path.resolve(__dirname, "../utils"),
    "@/units": path.resolve(__dirname, "../units"),
    "@/primitives": path.resolve(__dirname, "../primitives"),
    "@/translations": path.resolve(__dirname, "../translations"),
    "@/stories": path.resolve(__dirname, "../stories"),
    "@/tokens": path.resolve(__dirname, "../tokens"),

  };
  
  return config;
};