# PROVIDERS

Some providers are declared once: `I18nProvider` and `OverlayProvider` are consumed in `_app.js` under `AppProviders`. You may add more...

`SSRProvider` must be present on a `page` that contains `@react-aria` packages and can be imported as `PageProviders`. SEE TODO

`AuthProvider` must be present used on private `page` (it includes `SSRProvider`). SEE TODO

`PageProvidersWithParallax` must be declared on every `page.js` using such fx (it includes `SSRProvider`).

TODO: test if them can be used in `_document.js` or `BaseLayout`?
