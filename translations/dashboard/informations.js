export const CONTENT = Object.freeze({
  "en-EN": {
    title: "UTMB World",
    greeting: "Hello, {name}!",
  },
  "fr-FR": {
    title: "UTMB World",
    greeting: "Bonjour, {name}!",
  },
  "es-ES": {
    title: "UTMB World",
    greeting: "Hola, {name}",
  },
});
