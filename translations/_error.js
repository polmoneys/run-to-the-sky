export const CONTENT = Object.freeze({
  "en-EN": {
    server: "An error {statusCode} occurred on server",
    client: "An error occurred on the client",
  },
  "fr-FR": {
    server: "An error {statusCode} occurred on server",
    client: "An error occurred on the client",
  },
  "es-ES": {
    server: "Un error {statusCode} en el servidor",
    client: "Un error en el cliente",
  },
});
