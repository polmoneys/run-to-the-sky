const toastIds = {
  // APP
  internetOnline: "internet-online",
  internetOffline: "internet-offline",
  retryInternet: "internet-retry",
};
export const CONTENT = Object.freeze({
  "en-US": {
    greeting: "Hello, {name}!",
  },
  "fr-FR": {
    greeting: "Bonjour, {name}!",
  },
});

export const TOAST_MESSAGES = Object.freeze({
  0: [
    {
      concept: "api",
      message: {
        en: "Fail",
        fr: "",
        es: "",
      },
    },
    {
      concept: "user",
      message: {
        en: "Fail",
        fr: "",
        es: "",
      },
    },
  ],
  1: [
    {
      concept: "api",
      message: {
        en: "Success",
        fr: "",
        es: "",
      },
    },
    {
      concept: "user",
      message: {
        en: "Success",
        fr: "",
        es: "",
      },
    },
  ],
  2: [
    {
      concept: "api",
      message: {
        en: "Atention",
        fr: "",
        es: "",
      },
    },
    {
      concept: "user",
      message: {
        en: "Atention",
        fr: "",
        es: "",
      },
    },
  ],
  3: [
    {
      concept: "catch-all",
      message: {
        en: "Unknown error",
        fr: "",
        es: "",
      },
    },
  ],
});
