export const CONTENT = Object.freeze({
  "en-EN": {
    title: "Hello!",
    greeting: "Hello, {name}!",
  },
  "en-US": {
    title: "Hello!",
    greeting: "Hello, {name}!",
  },
  "fr-FR": {
    title: "Bonjour",
    greeting: "Hello, {name}!",
  },
  "es-ES": {
    title: "Hola",
    greeting: "Hello, {name}!",
  },
});
