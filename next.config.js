module.exports = () => {
  return {
    i18n: {
      locales: ["fr", "es", "en"],
      defaultLocale: "en",
    },
    env: {
      enableApiMocking: process.env.NODE_ENV === "development",
    },
    images: {
      domains: ["robohash.org"],
    },
  };
};
